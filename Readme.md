# Package seesaw
[![DOI](https://doi.org/10.1038/s41598-018-31688-9)](https://doi.org/10.1038/s41598-018-31688-9)

Cell Differentiation Modeling for Predicting Lineage Specifiers

## Installation manual 

### Dependencies
- R (>=3.4)
- GRNOpt package (>= 0.3.2), 

To install the GRNOpt package, please follow the instructions at: https://git-r3lab.uni.lu/andras.hartmann/GRNOptR/

To clone the repository you can use e.g.

```bash
$ git clone ssh://git@git-r3lab-server.uni.lu:8022/andras.hartmann/seesaw.git
```

Then you can install the package from the source.

```R
> install.packages(path_to_file, repos = NULL, type="source")
```
