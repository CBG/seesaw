#' Gets GEO experiment
#'
#' @param GEO_IDs list of GEO identifier
#' @param valname string identifying the values
#' @return loaded microarray data
#' @author Andras Hartmann
#' @keywords GEO
#' @keywords data
#' @keywords load
#' @examples Usage: gsm <- getExperiment("GSM720404")

getGEOExperiment <- function(GEO_IDs, valname = "VALUE" ){

  library(GEOquery)

  #FIXME: Only works for mouse db ...
  library(mouse430a2.db)
  library(mogene10sttranscriptcluster.db)
  library(mogene11sttranscriptcluster.db)
  library(AnnotationDbi)

  for (i in 1:length(GEO_IDs)){
    #Download data from GEO
    #TODO: more sophisticated tempdir
    gsm <- getGEO(GEO_IDs[i], destdir = '/tmp')
    gsm <- getGEO(GEO_IDs[i] )
    t <- Table(gsm)[,c("ID_REF","VALUE")]
    if (i==1){
      data = t
      #colnames(data) <- c("REF_ID", "SYMBOL")
    } else {
      data_ <-  merge(data,t, by="ID_REF")
      data = data_
    }

  }
  if (Meta(gsm)$platform == "GPL1261") {
    anndb = mouse430a2.db
  }
  if (Meta(gsm)$platform == "GPL6246") {
    anndb = mogene10sttranscriptcluster.db
  }

  if (Meta(gsm)$platform == "GPL11533") {
    anndb = mogene11sttranscriptcluster.db
  }
  #FIXME: for now, let's remove multiple elements from one to many (multivals=filter)
  sym <- AnnotationDbi::select(anndb,keys= as.character(data$ID_REF), columns=c("SYMBOL"))
  #This does not work ... don't know the keytype
  #sym <- mapIds(mouse430a2.db,keys= as.character(data$ID_REF), column="SYMBOL", multiVals="filter")
  colnames(sym) <- c("ID_REF", "SYMBOL")
  #We just remove NAs
  sym = sym[which(!is.na(sym$SYMBOL)),]

  data_ <-  merge(sym, data, by="ID_REF")
  data = data_

  ind = startsWith(colnames(data),"VALUE")
  #TODO: rename columns ...
  cnames = colnames(data)
  cnames[ind] = valname
  colnames(data)<-cnames

  #Computing the variance
  data$var = apply(data[,ind],1,var)

  #Duplicated
  dup = unique(data$SYMBOL[duplicated(data$SYMBOL)])

  data$to_del = FALSE

  #mark rows to delete
  for (i in 1:length(dup)){
    ind <- which(data$SYMBOL == dup[i])
    if(length(ind)>0){
      #initialize patterns to TRUE
      pattern <- !vector(mode="logical", length=length(ind))
      #set first max pattern to FALSE
      pattern[ which(data$var[ind]==max(data$var[ind]))[[1]] ] = FALSE
      data$to_del[ind] <-pattern
    } else {
      print("How can this happen?")
    }

  }

  #Delete the ones marked
  data = data[which(!data$to_del),]

  #remove unnecessary columns
  data$var = NULL
  data$to_del = NULL
  data$ID_REF = NULL

  #bioconductor packages: https://gist.github.com/seandavi/bc6b1b82dc65c47510c7#file-platformmap-txt
  #https://www.bioconductor.org/packages/release/data/annotation/
  # rownames(DB) <- as.character(EntrezID)
  #convert probes to gene names
  # (Meta(gsm))$platform
  #GPL can be queried with geoquery

  return(data)
}
