#' Discovers largest strongly connected component (SCC)
#'
#' Finds largest SCCs (Strongly Connected Components) in the network
#' Uses Graph::Directed Perl module
#' (http://search.cpan.org/dist/Graph/lib/Graph.pod) to discover strongly
#'  connected components, and checks if the pairs are contained in them
#'
#' @param B: List of best obtained solutions
#' @param T: gene pairs to consider
#' @param callback: callback function to call with the increment (a value between 0 and 1), used for progress bar
#' @return gene pairs mostly enriched in SCCs
#' @keywords SCC
#' @keywords network
#' @author Andras Hartmann
#' @export

## Find SCCs
findSCCs<- function(B, T, bool_data, callback=NULL) {
  if (!is.list(B)){
    B = list(B)
  }
  library("GRNOpt")

  #Command to run scc detection (perl script)
  SCC_command = system.file("perl", "findSCC.pl", package = "seesaw")
  scc = list()
  for (i in 1:length(B)){
    scc[[i]] = getSCCs(B[[i]])

    if(!is.null(callback))
    {
      callback(0.8/length(B))
    }

  }

  ## For each SCC, check the occurrence of seesaw pairs in each SCC #
  seesaw = toupper(rownames(T))
  # Check if both genes are present in each SCC
  scc.fraction = lapply(seesaw, function(x){
    s = strsplit(x,"__")
    #Initialize counters
    count = 0
    count.outdegree = 0
    count.direct.connection = 0
    if (!is.na(s[[1]][1]))
    {
      for(i in 1:length(scc)){
        genes = unique(c(scc[[i]][,1], scc[[i]][,3]))
        if(sum(s[[1]] %in% genes)==2){
          count = count+1
        }
        if( any(scc[[i]][,1] == s[[1]][1] & scc[[i]][,3] == s[[1]][2]) || any (scc[[i]][,3] == s[[1]][1] & scc[[i]][,1] == s[[1]][2])


        ){
          count.direct.connection = count.direct.connection + 1
        }

        count.outdegree = count.outdegree + min(length(which(B[[i]][,1] == s[[1]][1])), length(which(B[[i]][,1] == s[[1]][2])))

      }
    }
    c(count / length(scc)*100, count.direct.connection / length(scc)*100, count.outdegree/length(scc) )
  })
  names(scc.fraction) = seesaw

  ## Combine seesaw fraction and SCC fraction
  scc.pairs = do.call('rbind',lapply(scc.fraction,function(x){x}))
  colnames(scc.pairs) = c('scc.fraction','direct.connection', 'min.outdegree')

  ## Check the ratioDifference
  scc.pairs = cbind(scc.pairs, T[match( rownames(scc.pairs), rownames(T)),])
  scc.pairs = scc.pairs[order(-scc.pairs[,1], -scc.pairs[,2]), ]

    if(!is.null(callback))
    {
      callback(0.1)
    }
  return(scc.pairs)

}
