################################
## 1. NSC vs neurons and glia ##
################################
## 1. Jens data ##
setwd("/home/satoshi/new/perspective/neural_data/fromThomas_Palm")
library(oligo)
library(pd.hugene.1.0.st.v1)
library(mogene10stt.db)
b <- read.celfiles(list.celfiles())

## Normalization
brma=rma(b)
library(geneplotter)
multidensity(exprs(brma))
# Option 2: quantile/vsn

## Add gene symbol
M = exprs(brma)
colnames(M)=sub("\\.CEL$","", colnames(M))
sm <- unlist(mget(rownames(M), mogene10sttranscriptclusterSYMBOL, ifnotfound=NA))
M=cbind('Gene.Symbol'=sm,M)
write.table(M, "/home/satoshi/new/perspective/neural_data/data_fromThomas_Palm.csv",sep="\t",row.names=TRUE,quote=FALSE)


## 2. GSE9566 ##
setwd("/home/satoshi/new/perspective/neural_data/GSE9566")
library(affy)
b=ReadAffy()

## Normalization
library(geneplotter)
multidensity(log2(b))
# Option 1: (RMA is only for affy)
brma=rma(b)
multidensity(exprs(brma))
# Option 2: quantile/vsn

## Add gene symbol
M = exprs(brma)
colnames(M)=sub("\\.CEL$","", colnames(M))
id_table = read.csv("GSE9566_family.soft.table",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
M=cbind('Gene.Symbol'=id_table$Gene.Symbol[match(rownames(M),id_table$ID)],M)
write.table(M, "/home/satoshi/new/perspective/neural_data/data_GSE9566.csv",sep="\t",row.names=TRUE,quote=FALSE)


#############################
## Combine all and export  ##
## 1. Jens data
jens = read.csv("/home/satoshi/new/perspective/neural_data/data_fromThomas_Palm.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
## 2. GSE9566
D2 = read.csv("/home/satoshi/new/perspective/neural_data/data_GSE9566.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)

a=jens[match(D2[,1], jens[,1]),]
A = cbind(D2, a[,-1])
write.table(A, "/home/satoshi/new/perspective/neural_data/data_nscSytem.csv",sep="\t",row.names=TRUE,quote=FALSE)

##########################
## Global normalization ##
#data=read.csv("/home/satoshi/new/perspective/neural_data/data_fromThomas_Palm.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
#library(geneplotter)
#multidensity(data[,-1])
#RowName=data[,1]
#data=data[,-1]

## Global normalization
# Option 1: quantile normalization
#library(limma)
#dq=normalizeQuantiles(data)
#multidensity(dq)
# Option 2: median shifting
#v2=apply(data,2,function(x){ x - median(x,na.rm=TRUE) })
# Option 3: MAD
#v3=apply(data,2,function(x){ (x - median(x,na.rm=TRUE)) / mad(x,na.rm=TRUE) })
#pdf("/home/satoshi/new/perspective/esc/normalization.pdf")
#par(mfrow=c(3,1))
#multidensity(dq,legend='topleft',xlim=c(-7,20))
#multidensity(v2,legend='topleft',xlim=c(-7,20))
#multidensity(v3,legend='topleft',xlim=c(-7,20))
#graphics.off()
#write.table(dq, "/home/satoshi/new/perspective/esc/nuria/data_escSystem_nuria_quantile.csv",sep="\t",row.names=TRUE,quote=FALSE)
#write.table(RowName, "/home/satoshi/new/perspective/esc/nuria/RowName.csv",sep="\t",row.names=TRUE,quote=FALSE)

##############
## Analysis ##
##############
## Load all necessary files
## 1. Jens data
#data=read.csv("/home/satoshi/new/perspective/neural_data/data_fromThomas_Palm.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
## 2. GSE9566 + jens
data = read.csv("/home/satoshi/new/perspective/neural_data/data_nscSytem.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
RowName=data[,1]
data=data[,-1]
sample_table = read.csv("/home/satoshi/new/perspective/neural_data/sample_table_NSC.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

## Option 1: 2013 paper
#tfs=read.csv("/home/satoshi/new/perspective/tf_symbols.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
## Option 2: Riken database (http://genome.gsc.riken.jp/TFdb/tf_list.html)
#tfs = read.csv("/home/satoshi/new/perspective/riken_mouseTFs.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
## Option 3: bioguo (http://www.bioguo.org/AnimalTFDB/download/gene_list_of_Mus_musculus.txt)
tfs = read.csv("/home/satoshi/new/perspective/bioguo_mouseTFs.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

## Get this list from the mirror .xls file to avoid font degradation
keyTF = read.csv("/home/satoshi/new/perspective/neural_data/keyTFs_NSC.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")


## Making files for Malai's network inference tool
# Option 1: mixture
#data = data[,-c(45,46)]
data = data[,-c(41,42,45,46,47,48,49,50)] # remove jens's neuron and glia
colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']
# 1. NSC
edata = data[,which(colnames(data) %in% c('NSC'))]
# 2. Neuron
edata = data[,which(colnames(data) %in% c('Neuron'))]
# 3. Astroglia
edata = data[,which(colnames(data) %in% c('Astroglia'))]

# Option 2: Jens only
data = data[,c(39:52)]
colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']
# 1. NSC
edata = data[,which(colnames(data) %in% c('NSC'))]
# 2. Neuron
edata = data[,which(colnames(data) %in% c('Neuron'))]
# 3. Astroglia
edata = data[,which(colnames(data) %in% c('Glia'))]

variance = apply(edata,1,var,na.rm=TRUE); names(variance)=RowName
variance.aggregated.index = tapply(seq_along(variance), names(variance), function(x){
  m = max(variance[x])
  I = which(variance[x]==m)
  x[I[1]]
})
variance.aggregated.index = variance.aggregated.index[!is.na(variance.aggregated.index)]
edata.aggregated = edata[variance.aggregated.index,]
rownames(edata.aggregated) = RowName[variance.aggregated.index]
tfs = read.csv("/home/satoshi/new/perspective/tf_symbols.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
a = edata.aggregated[rownames(edata.aggregated) %in% tfs[,3],]

# 1. NSC
write.table(a, "/home/satoshi/new/perspective/neural_data/expression_edgeInference_NSC.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
# 2. Neuron
write.table(a, "/home/satoshi/new/perspective/neural_data/expression_edgeInference_Neuron.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
# 3. Astroglia
write.table(a, "/home/satoshi/new/perspective/neural_data/expression_edgeInference_Astroglia.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)

# Jens. Glia
write.table(a, "/home/satoshi/new/perspective/neural_data/expression_edgeInference_Astroglia.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)


#############################
## Hierarchical clustering ##
#############################
require(gplots)
require(made4)

## Optional: normalize against Gapdh
A=t(data)
B=t(A * 1/A[,'Gapdh'])
data = as.data.frame(B)

## Genes of interest
ids = c('Gata3','T','Nanog','Pou5f1','Sox2','Hey1', 'Olig2', 'Sox9', 'Sox8', 'Emx2', 'Cebpb', 'Lmo4', 'Nkx2-2')
ids = g1[!is.na(g1)]
I = match(ids,RowName)
a=t(data[I,])
colnames(a) = ids
#rownames(a) = sample_table[match(rownames(a),sample_table[,'Sample_source_name_ch1']),'Sample_class']
rownames(a) = sample_table[match(rownames(a),sample_table[,'ID_REF']),'Sample_class']
a=a[complete.cases(a),]

## Add numbers to duplicated row names
sprintf("NSC%d", 1:length(which(rownames(a) %in% 'NSC')))
sprintf("Neuron%d", 1:length(which(rownames(a) %in% 'Neuron')))
v = rownames(a)
sapply(unique(rownames(a)),function(x){
  I = which(rownames(a) %in% x)
  v[I] <<- sprintf("%s%d", x,1:length(I))
})
rownames(a)=v

## Heatmap
pdf("/home/satoshi/new/perspective/esc/heatmap_ESC_system.pdf")
heatplot(a, dend='none')
heatplot(a)
heatmap.2(a, trace="none", Colv=T,Rowv=T)
heatmap.2(d[1:100,], Rowv=T, Colv=F, dendrogram=c("none"), breaks=breaks, col=colours, symbreaks=T, trace="none", density.info="none", rowsep=nrow(a)+1,cexRow=1, cexCol=2)
graphics.off()

################
## Limma test ##
################
data = read.csv("/home/satoshi/new/perspective/neural_data/data_nscSytem.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
RowName = data[,1]
data = data[,-1]
sample_table = read.csv("/home/satoshi/new/perspective/neural_data/sample_table_NSC.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

## Option 1. Jens
#data = data[,-c(7,8)]
## Option 1: Remove only Glia
#data = data[,-c(9,10,11,12),drop=FALSE]
## Option 2: Remove only Neuron
#data = data[,-c(3,4,7,8),drop=FALSE]
## Option 3: Remove only NSC
#data = data[,-c(1,2,5,6),drop=FALSE]
## Jens
#colnames(data) = sample_table[match(colnames(data),sample_table[,'Sample_source_name_ch1']),'Sample_class']
#smoothScatter(rowMeans(data[,colnames(data)=='Neuron']),rowMeans(data[,colnames(data)=='Glia']))
# Option 1: Jens only
#data = data[,c(39:52)]
#colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']

## Option 2. GSE9566
#data = data[,-c(45,46)]
data = data[,-c(41,42,45,46,47,48,49,50)] # remove jens's neuron and glia
colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']


# 1. Only NSC and Neuron
#data = data[,which(colnames(data) %in% c('NSC','Neuron'))]
#colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
#smoothScatter(rowMeans(data[,colnames(data)=='NSC']),rowMeans(data[,colnames(data)=='Neuron']))
# 2. Only NSC and Astroglia
#data = data[,which(colnames(data) %in% c('NSC','Astroglia'))]
#colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
#smoothScatter(rowMeans(data[,colnames(data)=='NSC']),rowMeans(data[,colnames(data)=='Astroglia']))
# 3. Only Neuron and Astroglia
data = data[,which(colnames(data) %in% c('Neuron','Astroglia'))]
colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
# 4 (jens). Only Neuron and Glia
#data = data[,which(colnames(data) %in% c('Neuron','Glia'))]
#colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
#smoothScatter(rowMeans(data[,colnames(data)=='Neuron']),rowMeans(data[,colnames(data)=='Glia']))

## Export intensity and variance files
data = data[,colnames(data) %in% c('NSC','Neuron','Astroglia','Glia')]
colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
# 1. NSC
e1 = rowMeans(data[,colnames(data)=='NSC']); names(e1)=RowName
write.table(as.matrix(e1), "/home/satoshi/new/perspective/neural_data/intensity_GSE9566_NSC.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
v1 = apply(data[,colnames(data)=='NSC'],1,var); names(v1)=RowName
write.table(as.matrix(v1), "/home/satoshi/new/perspective/neural_data/variance_GSE9566_NSC.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
# 2. Neuron
e2 = rowMeans(data[,colnames(data)=='Neuron']); names(e2)=RowName
write.table(as.matrix(e2), "/home/satoshi/new/perspective/neural_data/intensity_GSE9566_Neuron.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
v2 = apply(data[,colnames(data)=='Neuron'],1,var); names(v2)=RowName
write.table(as.matrix(v2), "/home/satoshi/new/perspective/neural_data/variance_GSE9566_Neuron.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
# 3. Astroglia
e3 = rowMeans(data[,colnames(data)=='Astroglia']); names(e3)=RowName
write.table(as.matrix(e3), "/home/satoshi/new/perspective/neural_data/intensity_GSE9566_Astroglia.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
v3 = apply(data[,colnames(data)=='Astroglia'],1,var); names(v3)=RowName
write.table(as.matrix(v3), "/home/satoshi/new/perspective/neural_data/variance_GSE9566_Astroglia.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)

##########################
## Global normalization ##
##########################
data = read.csv("/home/satoshi/new/perspective/neural_data/data_nscSytem.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
RowName = data[,1]
data = data[,-1]
sample_table = read.csv("/home/satoshi/new/perspective/neural_data/sample_table_NSC.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
#tfs = read.csv("/home/satoshi/new/perspective/tf_symbols.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
## Option 3: bioguo (http://www.bioguo.org/AnimalTFDB/download/gene_list_of_Mus_musculus.txt)
tfs = read.csv("/home/satoshi/new/perspective/bioguo_mouseTFs.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

## Get this list from the mirror .xls file to avoid font degradation
keyTF = read.csv("/home/satoshi/new/perspective/neural_data/keyTFs_NSC.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

## PCA
pdf('pca_JensData.pdf')
# Option 1: Jens
#data = data[,c(39:52)]
#colnames(data) = c('NSC1','NSC2','d2_Neuron1','d2_Neuron2','NSC3','NSC4','d2_Neuron3','d2_Neuron4','d5_Neuron1','d5_Neuron2','Glia1','Glia2','Glia_Schwamborn1','Glia_Schwamborn2')
# Option 2: GSE9566
data = data[,-c(20, 33,39:52)]
colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']
# centring
M = data[complete.cases(data),]
cent.M2 = sweep(M,2,apply(M,2,mean))
# scaling
cent.M2 = sweep(cent.M2,2,apply(cent.M2,2,sd),FUN='/')
#multidensity(cent.M2)
# Option 1: princomp
pc = princomp(cent.M2)
#biplot(pc, choices=c(1,2),cex=0.6,main="All proteins")
plot(pc$loadings)
# Option 1:
#text(pc$loadings[-c(7,11),1],pc$loadings[-c(7,11),2],rownames(pc$loadings)[-c(7,11)],cex=0.7, pos=3, col="red")
#text(pc$loadings[c(7,11),1],pc$loadings[c(7,11),2],rownames(pc$loadings)[c(7,11)],cex=0.7, pos=4, col="red")
# Option 2:
text(pc$loadings[,1],pc$loadings[,2],rownames(pc$loadings),cex=0.7, pos=3, col="red")
graphics.off()
#########

## Make file for ratio difference statistics ##
data = data[,-c(20,33,41,42,45:52)]
colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']
data = data[,which(colnames(data) %in% c('NSC','Neuron','Astroglia'))]
colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)

# Option 1: quantile
data = normalizeQuantiles(data)
# Option 2: vsn
#library(vsn)
#v = vsn2(as.matrix(2**data))
#data = exprs(v)
multidensity(data)

data = cbind(RowName,data)
write.table(data, "/home/satoshi/new/perspective/neural_data/quantile_jensNSC_GSE9566Neuron-Astroglia.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=TRUE)
###############################################


## Option 1: Jens only
#data = data[,c(39:52)]
#colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']
# Option 1: Remove outlier glia and d5_neuron
#data = data[,-c(9,10,11)]; colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
# Option 2: Remove outlier glia and d2_neuron
#data = data[,-c(3,4,7,8,11)]; colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
# Take only neuron and glia
#data = data[,which(colnames(data) %in% c('Neuron','Glia'))]
#colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
## Option 2. GSE9566
data = data[,-c(20,33,39:52)]
# Option: remove day<10 samples
#data = data[,-which(colnames(data) %in% c('GSM241899','GSM241901','GSM241903','GSM241922','GSM241905','GSM241906','GSM241907','GSM241937','GSM241898','GSM241900','GSM241902','GSM241920','GSM241928','GSM241936'))]
colnames(data) = sample_table[match(colnames(data),sample_table[,'ID_REF']),'Sample_class']
# Option 2-1
#data = data[,which(colnames(data) %in% c('Neuron','Astroglia','Astrocyte'))]
# Option 2-2
#data = data[,which(colnames(data) %in% c('Neuron','Astrocyte'))]
# Option 2-3
data = data[,which(colnames(data) %in% c('Neuron','Astroglia'))]
colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)

library(geneplotter)
multidensity(data)
library(limma)
# Option 1: quantile
#data = normalizeQuantiles(data)
# Option 2: vsn
library(vsn)
v = vsn2(as.matrix(2**data))
data = exprs(v)
multidensity(data)

## Intensity
E = cbind(rowMeans(data[,colnames(data)=='Neuron']), rowMeans(data[,colnames(data)!='Neuron'])); rownames(E)=RowName
smoothScatter(E[,1],E[,2])
plot(E[,1],E[,2],pch=20,cex=0.5)

## Design matrix
library(limma)
f = sample_table[match(colnames(data),sample_table[,2]),2]
f <- factor(f, levels=unique(f))
design <- model.matrix(~0+f)
colnames(design) <- levels(f)
colSums(design)
## Comparisons
## 1. Jens
# 1. NSC.vs.Neuron
#cont.matrix <- makeContrasts(NSC.vs.Neuron = NSC - Neuron, levels = design)
# 2. NSC.vs.Glia
#cont.matrix <- makeContrasts(NSC.vs.Glia = NSC - Glia, levels = design)
# 3. Neuron.vs.Glia
#cont.matrix <- makeContrasts(Neuron.vs.Glia = Neuron - Glia, levels = design)

## 2. GSE9566
#cont.matrix <- makeContrasts(GNS.vs.NS = (G25 + G144 + G166 + G179) / 4 - (CB152 + CB660) / 2, levels = design)
# 1. Neuron vs Astroglia
cont.matrix <- makeContrasts(Neuron.vs.Astroglia = Neuron - Astroglia, levels = design)
# 2. Neuron vs Astrocyte
#cont.matrix <- makeContrasts(Neuron.vs.Astrocyte = Neuron - Astrocyte, levels = design)
# 3. Neuron vs Astroglia+Astrocyte
#cont.matrix <- makeContrasts(Neuron.vs.Glia = Neuron - (Astroglia+Astrocyte), levels = design)
# 4 (jens). Neuron vs Glia
#cont.matrix <- makeContrasts(Neuron.vs.Glia = Neuron - Glia, levels = design)

##  Intensity binning
Intensity = rowMeans(data,na.rm=TRUE)
K = 30
library(limma)
q = quantile(Intensity,probs=seq(0,1,length.out=K+1))
I = cut(Intensity, breaks=q,labels=FALSE)
I[is.na(I)] = 1
pval = rep(NA_real_,nrow(data))
padj = rep(NA_real_,nrow(data))
for (k in 1:K) {
  fit = lmFit(data[I==k,], design)
  fit2 <- eBayes( contrasts.fit(fit, cont.matrix) )  
  pval[I == k] = fit2$p.value
  padj[I == k] = p.adjust(fit2$p.value,method="BH")
  print(sum(padj[I == k] <= 0.05,na.rm=TRUE))
}
names(padj)=RowName
hist(padj); length(padj)

## Fold change
## 1. Jens
# 1. NSC.vs.Neuron
#fold = rowMeans(data[,colnames(data)=='NSC']) - rowMeans(data[,colnames(data)=='Neuron']); names(fold)=RowName
# 2. NSC.vs.Glia
#fold = rowMeans(data[,colnames(data)=='NSC']) - rowMeans(data[,colnames(data)=='Glia']); names(fold)=RowName
#3 Neuron.vs.Glia
#fold = rowMeans(data[,colnames(data)=='Neuron']) - rowMeans(data[,colnames(data)=='Glia']); names(fold)=RowName

## 2. GSE9566
# 1.
#fold = rowMeans(data[,colnames(data)=='NSC']) - rowMeans(data[,colnames(data)=='Neuron']); names(fold)=RowName
# 2.
#fold = rowMeans(data[,colnames(data)=='NSC']) - rowMeans(data[,which(colnames(data) %in% c('Astroglia'))]); names(fold)=RowName
# 3.
fold = rowMeans(data[,colnames(data)=='Neuron']) - rowMeans(data[,which(colnames(data) %in% c('Astroglia','Astrocyte'))]); names(fold)=RowName


## Aggregate genes by highest variance across samples
## 1. Jens
#variance = apply((data[,which(colnames(data) %in% c('NSC','Neuron','Glia'))]),1,var); names(variance)=RowName
## 2. GSE9566
variance = apply((data[,which(colnames(data) %in% c('NSC','Neuron','Glia','Astrocyte','Astroglia'))]),1,var,na.rm=TRUE); names(variance)=RowName

variance.aggregated.index =tapply(seq_along(variance), names(variance), function(x){
  m = max(variance[x])
  I = which(variance[x]==m)
  x[I[1]]
})
## Use this index for padj and fold
padj.aggregated = padj[variance.aggregated.index]
fold.aggregated = fold[variance.aggregated.index]
a = cbind(padj.aggregated, fold.aggregated)
a = a[complete.cases(a),]
a[rownames(a) %in% c('Neurog1','Neurog2','Hes1','Notch','Prox1','Ascl1','Gfap','Nes','Dclk2','Dclk2','Tubb3','Prom1','Ncam1','Cd44','A2b5','Mtap2','Olig2','Nfel'),,drop=FALSE]


## Aggregate pvalues by mean
#padj.aggregated = tapply(padj, names(padj), mean) 
#hist(padj.aggregated); length(padj.aggregated)
## Aggregate fold change by mean
#fold.aggregated = tapply(fold, names(fold), mean) 
#hist(fold.aggregated); length(fold.aggregated)

## For Isaac's tool
fc = fold.aggregated
fc = fc[-grep("///",names(fc))]
names(fc)=toupper(names(fc))
fc = as.matrix(fc)
fc = ifelse(fc<0, 'DOWN', 'UP')
# Remove illegal names
fc = cbind(rownames(fc)[-grep( ("\\.\\d|.+?-.+?-.+?|^$|\\("), rownames(fc),perl=TRUE)] , fc[-grep( ("\\.\\d|.+?-.+?-.+?|^$|\\("), rownames(fc),perl=TRUE)] )
fc = fc[complete.cases(fc),]

## 1. Jens
# 1. NSC.vs.Neuron
#write.table(fc, "updown_SCC__jens_NSC-Neuron.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 2. NSC.vs.Glia
#write.table(fc, "updown_SCC__jens_NSC-Glia.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3. Neuron.vs.Glia
#write.table(fc, "updown_SCC__jens_Neuron-Glia.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
## 2. GSE9566
# 1. NSC.vs.Neuron
#write.table(fc, "updown_SCC__GSE9566_NSC-Neuron.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 2. NSC.vs.Glia
#write.table(fc, "updown_SCC__GSE9566_NSC-Astroglia.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3. Neuron.vs.Astroglia
write.table(fc, "updown_SCC__GSE9566_Neuron-Astroglia.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)


## Volcano plot
plot(fold.aggregated,-log10(padj.aggregated),col=ifelse(padj.aggregated <= 0.05 & abs(fold.aggregated)>=1, "red", "black"), pch=20,main="GNS/NS",xlab="log2(mean ratio)", ylab="-log10(adjusted p-value)",cex.axis=1.3,cex.lab=1.3,cex=0.5)

## Check key TF padj
round(padj[which(names(padj) %in% keyTF[,1])],digit=3)

## Check DE TFs
g1 = names(padj.aggregated)[(which(padj.aggregated<=0.05 & abs(fold.aggregated)>=1))]
length(unique(g1))
g1.tf = g1[which(g1 %in% tfs[,'mgi_symbol'])]
g1.tf = unique(g1.tf[!is.na(g1.tf)]); length(g1.tf)
g1.tf[which(g1.tf %in% keyTF[,1])]

## 1. Jens
# 1. NSC-Neuron
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNSCandNeuron__jens_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 2. NSC-Glia
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNSCandGlia__jens_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 3. Neuron-Glia
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNeuronandGlia__jens_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
## 2. GSE9566
# 1. NSC-Neuron
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNSCandNeuron__GSE9566_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 2. NSC-Glia
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNSCandAstroglia__GSE9566_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 3. Neuron-Asgroglia
write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)


########################################################################
## Network analysis ##
######################
## 1. Ariadne
# After Getting an Ariadne network, retrieve 'Relation' column and run 'makeSIFfromAriadne.pl'

## 2. MetaCore
# Retrieve direct interactions and object list
# 1.
metacore.interaction = read.csv("/home/satoshi/new/perspective/neural_data/DEbetweenNSCAndNeuron_metacore_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/neural_data/DEbetweenNSCAndNeuron_metacore_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 2.
metacore.interaction = read.csv("/home/satoshi/new/perspective/neural_data/DEbetweenNSCAndAstroglia_metacore_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/neural_data/DEbetweenNSCAndAstroglia_metacore_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 3.
metacore.interaction = read.csv("/home/satoshi/new/perspective/neural_data/DEbetweenNeuronAndAstroglia_metacore_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/neural_data/DEbetweenNeuronAndAstroglia_metacore_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

# Remove undirected interactions and extract needed information
a = metacore.interaction[which( (metacore.interaction$Effect=='Activation' |metacore.interaction$Effect=='Inhibition') & metacore.interaction$Mechanism=='Transcription regulation'),c('Network.Object..FROM.','Effect','Network.Object..TO.','Mechanism')]

# Map objects to gene names
a = cbind(a,'Gene.Symbol.From'=toupper(metacore.object[match(a[,'Network.Object..FROM.'],metacore.object[,'Network.Object.Name']),'Gene.Symbol']))
a = cbind(a,'Gene.Symbol.To'=toupper(metacore.object[match(a[,'Network.Object..TO.'],metacore.object[,'Network.Object.Name']),'Gene.Symbol']))
a[,'Effect']=tolower(a[,'Effect'])

# add self-loops


# Export a .sif file for cytoscape
# 1.
write.table(a[,c('Gene.Symbol.From','Effect','Gene.Symbol.To')], "/home/satoshi/new/perspective/neural_data/metacore_nsc_directInteractions__GSE9566_NSCvsNeuron.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)
# 2.
write.table(a[,c('Gene.Symbol.From','Effect','Gene.Symbol.To')], "/home/satoshi/new/perspective/neural_data/metacore_nsc_directInteractions__GSE9566_NSCvsAstroglia.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)
# 3.
write.table(a[,c('Gene.Symbol.From','Effect','Gene.Symbol.To')], "/home/satoshi/new/perspective/neural_data/metacore_nsc_directInteractions__GSE9566_NeuronvsAstroglia.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)

## (optional) 3. Combine the two network files in the commmand line by '
# 'cat ariadneNetwork_combinations_DEbetweenNeuronAstrocyteglia_GSE9566_4ariadne.sif metacore_nsc_directInteractions_GSE9566.sif >xx'
# '(head -n 2 xx && tail -n +3 xx |sort -u) >yy'
# ' mv yy ariadneNetwork_metacore_DEbetweenNeuronAstrocyteglia_GSE9566.sif'

# Search for 'Hey1, Olig2, Sox9, Sox8, Emx2, Cebpb, Lmo4, Nkx2-2, Pax6, Olig1, Sox2, Hes5, Nkx6-1, Gli3, Gli2' in cytoscape


## Load it in cytoscape and find SCCs
## Format it for Isaac's tool
# 1.
T=read.csv("/home/satoshi/new/perspective/neural_data/SCC1__GSE9566_NSC-Neuron.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
T[,2]=ifelse(T[,2]=='activation','->', '-|')
T = T[!duplicated(T),]
write.table(T, "/home/satoshi/new/perspective/neural_data/arrow_SCC1__GSE9566_NSC-Neuron.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 2.
T=read.csv("/home/satoshi/new/perspective/neural_data/SCC1__GSE9566_NSC-Astroglia.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
T[,2]=ifelse(T[,2]=='activation','->', '-|')
T = T[!duplicated(T),]
write.table(T, "/home/satoshi/new/perspective/neural_data/arrow_SCC1__GSE9566_NSC-Astroglia.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3.
T=read.csv("/home/satoshi/new/perspective/neural_data/SCC1__GSE9566_Neuron-Astroglia.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
T[,2]=ifelse(T[,2]=='activation','->', '-|')
T = T[!duplicated(T),]
write.table(T, "/home/satoshi/new/perspective/neural_data/arrow_SCC1__GSE9566_Neuron-Astroglia.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

## Run Isaac's tool
## Copy and paste the contextualization from Isaac's tool

#################################
## Load contextualized network ##
#################################
net=read.csv("/home/satoshi/new/perspective/neural_data/arrow_SCC1_contextualized.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Filter by the average probability
net=cbind(net,'mean'=rowMeans(net[,2:ncol(net)]))
net = net[net[,'mean']>=0.5,]
n = do.call("rbind",lapply(strsplit(net[,1]," "),function(x){x}))
n[,2]=ifelse(n[,2]=='->','activation', 'inhibition')
a=n[,1:3]
## Cytoscape file for contextualized network
write.table(a, "/home/satoshi/new/perspective/neural_data/trimmed_arrow_SCC1_contextualized.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=TRUE)

## 2nd SCC finding ##
# Load the trimmed file into Cytoscape and run BiNom to find SCCs
# Optional: If there is >1 SCC, do hierarchicalization
# Export the SCC(s)

## Enumerate gene names ##
net=read.csv("/home/satoshi/new/perspective/neural_data/SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
enum = matrix(c(seq_along(unique(c(net[,1],net[,3]))), unique(c(net[,1],net[,3]))), ncol=2)
b = cbind(enum[match(net[,1],enum[,2]),1],enum[match(net[,3],enum[,2]),1])
length(unique(c(b[,1],b[,2]))) #unique node number 
b = cbind(b,net[,c(1,3)])
write.table(b, "/home/satoshi/new/perspective/neural_data/enum_SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

## Option 1: BiNom plugin
## Option 2: fanmod
# Fanmod/cycles_johnson_meyer format: onvert gene names into numbers
# Extract the first two columns by 'cut'
# Launch fanmod at ~/programmes/ext/fanmod
# Load the extracted file to fanmod and run it
# Export the html version and look at the images created
# How to check which specific nodes make the identified motifs?

## Option 3: cycles_johnson_meyer
# Extract the first two columns (numbers) in the command line
# Install cycles_johnson_meyer format (https://github.com/josch/cycles_johnson_meyer)
# Transfer the file to the directory '/home/satoshi/programmes/ext/cycles_johnson_meyer-master'
# Run with command line 'cat xx.txt |java de.normalisiert.utils.graphs.TestCycles numberOfNodes+1? >circuits.txt'

## Option 4: Malai's tool
# Extract the first two columns of the enum file by 'cut -f 1,2 -d ' ' enum_SCC1_trimmed_arrow_SCC1_contextualized.csv.sif >xx.txt'
# Convert the network into an adjacency matrix
el=read.csv("/home/satoshi/new/perspective/neural_data/xx.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
num=length(unique(c(el[,1],el[,2])))
mat=matrix(0,num,num)
mat[as.matrix(el)]=1
write.table(mat, "/home/satoshi/new/perspective/neural_data/adjacency_enum_SCC1_trimmed_arrow_SCC1_contextualized.dat",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# Transfer this file to the matlab directory
# Open Matlab
# Follow the 'processNetwork.m' script
# Transfer the matlab output file to the neural_data directory

## Convert the numbers into gene symbols
circuit=read.csv("/home/satoshi/new/perspective/neural_data/circuits_enum_SCC1_trimmed_arrow_SCC1_contextualized.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
net=read.csv("/home/satoshi/new/perspective/neural_data/enum_SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
map = unique(rbind(cbind(net[,1],net[,3]), cbind(net[,2],net[,4])))
v=apply(circuit,2,function(x){ map[match(x,map[,1]),2] })
write.table(v, "/home/satoshi/new/perspective/neural_data/symbol_circuits_enum_SCC1_trimmed_arrow_SCC1_contextualized.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

## Key genes 
# 'Hey1, Olig2, Sox9, Sox8, Emx2, Cebpb, Lmo4, Nkx2-2, Pax6, Olig1, Sox2, Hes5, Nkx6-1, Gli3, Gli2' in cytoscape
# After finding SCC, Gli3 and Oli1 are lost

##########################################################
## Compute retroactivity ranking (interface out-degree) ##
##    for each positive circuit within a SCC            ##
## 1. Compute adjacency matrix with 1=activation, 2=inhibition
net=read.csv("/home/satoshi/new/perspective/neural_data/SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
el=read.csv("/home/satoshi/new/perspective/neural_data/enum_SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
map = unique(rbind(cbind(el[,1],el[,3]), cbind(el[,2],el[,4])))
num=length(unique(c(net[,1],net[,3])))
mat=matrix(0,num,num)
rownames(mat)=colnames(mat)=map[,2]
# 1. Indices of activation
act = net[net[,2]=='activation',]
mat[cbind(match(act[,1],rownames(mat)),match(act[,3],colnames(mat)))]=1
# 2. Indices of inhibition
inh = net[net[,2]=='inhibition',]
mat[cbind(match(inh[,1],rownames(mat)),match(inh[,3],colnames(mat)))]=-1

## 2. Compute circuit retroactivity
circuit=read.csv("/home/satoshi/new/perspective/neural_data/symbol_circuits_enum_SCC1_trimmed_arrow_SCC1_contextualized.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
retro.circuit=apply(circuit,1,function(x){
  x=x[!is.na(x)]
  a=mat[match(x,rownames(mat)), match(x,colnames(mat))]
  c(sum(a==1),sum(a==-1),sum(a==1|a==-1))
})
retro.circuit=t(retro.circuit)
colnames(retro.circuit)=c('activation','inhibition','both')

## 3. Compute gene retroactivity within each circuit
retro.gene=apply(circuit,1,function(x){
  x=x[!is.na(x)]
  a=subset(mat, rownames(mat) %in% x,select=x)
  cbind('activation'=rowSums(a==1),'inhibition'=rowSums(a==-1),'both'=rowSums(a==1|a==-1))
})

## 4. Get circuits with key genes being high/low retroactive
keys = c('Hey1', 'Olig2', 'Sox9', 'Sox8', 'Emx2', 'Cebpb', 'Lmo4', 'Nkx2-2', 'Pax6', 'Olig1', 'Sox2', 'Hes5', 'Nkx6-1', 'Gli3', 'Gli2')
r.max = lapply(retro.gene,function(x){
  x=x[x[,'both']==max(x[,'both']),]
  id = rownames(x)[which(rownames(x) %in%  toupper(keys))]
  x=subset(x, rownames(x)==id, drop=FALSE)
  if(length(x)>0){x}
})
r.max[sapply(r.max, is.null)] <- NULL


# 'Hey1, Olig2, Sox9, Sox8, Emx2, Cebpb, Lmo4, Nkx2-2, Pax6, Olig1, Sox2, Hes5, Nkx6-1, Gli3, Gli2' in cytoscape



#######################################
## PBN files (for contextualization) ##
#######################################
circuit=read.csv("/home/satoshi/new/perspective/neural_data/circuits_enum_SCC1_trimmed_arrow_SCC1_contextualized.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
net=read.csv("/home/satoshi/new/perspective/neural_data/SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
el=read.csv("/home/satoshi/new/perspective/neural_data/enum_SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
map = unique(rbind(cbind(el[,1],el[,3]), cbind(el[,2],el[,4])))

## Once circuits are identified, for each circuit,
##   generate followings:
## 1. Adjacency matrix (1 is activation, -1 is inhibition)
c1 = circuit[20,]; c1=c1[!is.na(c1)]
c1g = map[match(c1,map[,1]),2]
c1net = net[which(net[,1] %in% c1g & net[,3] %in% c1g),]
num=length(unique(c(c1net[,1],c1net[,3])))
mat=matrix(0,num,num)
rownames(mat)=colnames(mat)=c1g
# 1. Indices of activation
act = c1net[c1net[,2]=='activation',]
mat[cbind(match(act[,1],rownames(mat)),match(act[,3],colnames(mat)))]=1
# 2. Indices of inhibition
inh = c1net[c1net[,2]=='inhibition',]
mat[cbind(match(inh[,1],rownames(mat)),match(inh[,3],colnames(mat)))]=-1
write.table(mat, "/home/satoshi/programmes/matlabs/pbn/adjacency_circuit1_SCC1_trimmed_arrow_SCC1_contextualized.dat",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

## 2. Probability matrix file
# Assign a probability to each edge
P=mat
P[which(mat==1,arr.ind=TRUE)] = 1
write.table(P, "/home/satoshi/programmes/matlabs/pbn/edge_probability_circuit1.dat",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

## 3. Indices of mat
ind = which(!is.na(mat),arr.ind=TRUE)
write.table(ind, "/home/satoshi/programmes/matlabs/pbn/matrix_index_circuit1.dat",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

## 4. Data



######################################
## BoolNet (after contextualization ##
######################################
## Compute attractors for the entire network and take 2 attractors
##    colsest to the experimental data assuming that all of them
##    must be opposite in the two phenotypes
updown=read.csv("/home/satoshi/new/perspective/neural_data/updown_SCC.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
updown=cbind(updown[,1],ifelse(updown[,2]=="UP",1,0))

## Option 1: Xpred (fixed interactions)
# Convert the entire network file into xpred format
a=read.csv("/home/satoshi/new/perspective/neural_data/arrow_SCC1_contextualized.sif",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
write.table(cbind(a[,1],ifelse(a[,2]=='activation',"->","-|"),a[,3]), '4xpred_arrow_SCC1_contextualized.txt', sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# Use Xpred
# Read in Xpred output (predicted expression)
xpred=read.csv("/home/satoshi/new/perspective/neural_data/Xpred_expression.exp",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
onoff = do.call('rbind',lapply(strsplit(xpred[,2]," "),function(x){x[2]}))
symbol = do.call('rbind',lapply(strsplit(xpred[,1],":"),function(x){x[1]}))
attState = cbind(symbol,ifelse(onoff=="ON",1,0))
deg = updown[match(attState[,1],updown[,1]),2]
score = sum(abs(as.numeric(deg) - as.numeric(attState[,2])),na.rm=TRUE)
names(score)=NULL
sort(score)

## Option 2: BoolNet (only less than 30 boolean functions)
## Convert network into boolean functions (for BoolNet)
net=read.csv("/home/satoshi/new/perspective/neural_data/arrow_SCC1_contextualized.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 1. Inhibition dominant
v=lapply(unique(net[,3]),function(x){
  a=net[net[,3] %in% x,]
  inh=NULL
  act=NULL
  if(sum(a[,2] %in% 'inhibition') > 0){
    inh = paste("(",paste(paste("!",a[a[,2]=="inhibition",1]),collapse=" & "),")",sep="")
  }
  if(sum(a[,2] %in% 'activation') > 0){
    act = paste("(",paste(paste(a[a[,2]=="activation",1]),collapse=" | "),")",sep="")
  }
  paste(x,", ",paste(c(inh, act), collapse=' & '),sep="")
})
# Export as txt
unlink("test2.txt")
lapply(v, write, "test2.txt", append=TRUE)

## Load the list
library(BoolNet)
## Change 
allNet=loadNetwork("/home/satoshi/new/perspective/neural_data/test2.txt")
allNet$genes[which(allNet$genes %in% "NKX2_2")]="NKX2-2"
deg = updown[match(allNet$genes,updown[,1]),2]
attr <- getAttractors(allNet, method="chosen", startStates=list(deg))
# Get all attractor states including cycles (simple attractors)
attStates = do.call("rbind",lapply(1:length(attr$attractors),function(x){getAttractorSequence(attr, x)}))
# Convert into strings
#attString = apply(attStates,1,function(x){ paste(x,collapse="")})
#deg = paste(updown[match(names(attStates),updown[,1]),2],collapse="")
# Rank the attractors by closeness to deg
score = apply(attStates,1,function(x){
  a=rbind(x,deg)
  sum(abs(as.numeric(a[1,]) - as.numeric(a[2,])))
})
names(score)=NULL
sort(score)
## Take the lowerst score one and invert the expresssion
att1 = attStates[which(score==min(score)),]
att2 = ifelse(att1==1,0,1)

###################################################
## Compute attractors for each circuit (BoolNet) ##
###################################################
circuit = read.csv("/home/satoshi/new/perspective/neural_data/circuits_enum_SCC1_trimmed_arrow_SCC1_contextualized.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
el = read.csv("/home/satoshi/new/perspective/neural_data/enum_SCC1_trimmed_arrow_SCC1_contextualized.csv.sif",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
map = unique(rbind(cbind(el[,1],el[,3]), cbind(el[,2],el[,4])))
updown = read.csv("/home/satoshi/new/perspective/neural_data/updown_SCC.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
## Make a sif file for each circuit
net[net=="NKX2-2"]="NKX2_2"
map[map=="NKX2-2"]="NKX2_2"
score = apply(circuit,1,function(x){
  cir <- makeSifFile(x, map)
  ## Make boolnet format
  v <- makeBoolNetFunctions.inhibitionDominant(cir)
  ## Export as txt
  unlink("boolnet_c1.txt")
  lapply(v, write, "boolnet_c1.txt", append=TRUE)
  
  ## Compute attractors
  library(BoolNet)
  if(length(v)>1){
    c1 = loadNetwork("/home/satoshi/new/perspective/neural_data/boolnet_c1.txt")
    attStates <- getAttractorStatesFromInitialState(c1, updown)
    ## If the initial expression state and computed attractor state are
    ##   different, this circuit is regulated by environment
    c(sum(abs(attStates[[1]] - attStates[[2]]))/nrow(attStates[[1]]), length(attStates[[1]]))
  }else{
    NA
  }
})

################################
## Case 1: No external input ###
################################
allAttractors = apply(circuit,1,function(x){
  cir <- makeSifFile(x, map)
  ## Make boolnet format
  v <- makeBoolNetFunctions.inhibitionDominant(cir)
  ## Export as txt
  unlink("boolnet_c1.txt")
  lapply(v, write, "boolnet_c1.txt", append=TRUE)
  
  ## Compute attractors
  library(BoolNet)
  if(length(v)>1){
    c1 = loadNetwork("/home/satoshi/new/perspective/neural_data/boolnet_c1.txt")
    attStates <- getAttractorStatesExhaustive(c1, updown)
    #attStates <- getAttractorStatesFromInitialState(c1, updown)
    attStates
  }else{
    NA
  }
})

## For each circuit, take two attractors that are exactly the opposite
opposite.attractors = lapply(allAttractors,function(x){
  x=x[[1]]
  ## Compute the element-wise sum of all combinations
  if(!is.null(nrow(x))){
    com = combn(1:nrow(x),2)
    m = apply(com,2,function(y){
      s=colSums(x[y,])
      if(all(s==1)){ x[y,] }
    })
    m[sapply(m, is.null)] <- NULL
    m
  }
})

## Find (un?)stable steady states where mutually exclusive genes
##   are both up (1)
# Take the indices of non-NULL 'opposite.attractors' 
I = do.call('rbind',lapply(seq_along(opposite.attractors),function(ind,list,names){
  if(!is.null(list[[ind]])){ ind }
},list=opposite.attractors, names=names(opposite.attractors)))

# Take all attractors in the indices and check if there is other
#   attractors. If so, return the entries
num = lapply(opposite.attractors[I],function(x){length(x)})
circuitWithMoreAttractors = list()
for (i in 1:length(num)){
  if(nrow(allAttractors[I][[i]][[1]]) > num[[i]]){
    circuitWithMoreAttractors[[i]] = allAttractors[I][[i]]
    circuitWithMoreAttractors[[i]]$OppositeAttractors = do.call("rbind",lapply(opposite.attractors[I][[i]],function(x){x}))
  }
}
# 'I' is the original indices

## Compute gene retroactivitities of these candidate attractors


##
## For each of these candidate states, make perturbation to see if
##   the state reaches one of the two opposite attractors. If so,
##   that perturbed gene is a candidate for the seesaw model
## Perturb only highest/lowest retroactive ones



########################################################################
## Compute the difference of ratios of all TF combinations ##
#############################################################
## Load the expression
e1 = read.csv("/home/satoshi/new/perspective/neural_data/intensity_GSE9566_NSC.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
v1 = read.csv("/home/satoshi/new/perspective/neural_data/variance_GSE9566_NSC.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
e2 = read.csv("/home/satoshi/new/perspective/neural_data/intensity_GSE9566_Neuron.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
v2 = read.csv("/home/satoshi/new/perspective/neural_data/variance_GSE9566_Neuron.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
e3 = read.csv("/home/satoshi/new/perspective/neural_data/intensity_GSE9566_Astroglia.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
v3 = read.csv("/home/satoshi/new/perspective/neural_data/variance_GSE9566_Astroglia.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
expression = cbind(cbind(e1, e2[match(rownames(e2), rownames(e1)),]), e3[match(rownames(e3), rownames(e1)),]) 
expression = expression[,-c(3,5)]
colnames(expression) = c('Symbol','NSC','Neuron','Astroglia')
# Change Sfpi1 to Spi1 to match Metacore
expression[expression[,'Symbol']=='Sfpi1','Symbol']='Spi1'
## Variance
expression.variance = cbind(cbind(v1, v2[match(rownames(v2), rownames(v1)),]), v3[match(rownames(v3), rownames(v1)),]) 
expression.variance = expression.variance[,-c(3,5)]
colnames(expression.variance) = c('Symbol','NSC','Neuron','Astroglia')
expression.variance[expression.variance[,'Symbol']=='Sfpi1','Symbol']='Spi1'

## Quantile normalization
library(limma)
expression[,2:4] = normalizeQuantiles(expression[,2:4])
library(geneplotter)
multidensity(expression[,2:4])


###################################################################
################ Using all individual samples #####################
###################################################################
## Load the expression
#expression = read.csv("/home/satoshi/new/perspective/neural_data/quantileMatrix_GSE9566Jens_NSC.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
expression = read.csv("/home/satoshi/new/perspective/neural_data/quantile_jensNSC_GSE9566Neuron-Astroglia.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
expression[expression[,'RowName']=='Sfpi1','RowName']='Spi1'
RowName = expression[,1]
#expression = expression[,-1]
library(limma)
expression[,-1] = normalizeQuantiles(expression[,-1])
colnames(expression) = gsub("\\.\\d+$","",colnames(expression),ignore.case=TRUE,perl=TRUE)

## Aggregate genes by highest variance across samples
variance = apply((expression[,which(colnames(expression) %in% c('NSC','Neuron','Astroglia'))]),1,var,na.rm=TRUE); names(variance)=RowName
variance.aggregated.index = tapply(seq_along(variance), names(variance), function(x){
  m = max(variance[x],na.rm=TRUE)
  I = which(variance[x]==m)
  x[I[1]]
})
## Use this index for padj and fold
expression = expression[variance.aggregated.index,]
rownames(expression) = RowName[variance.aggregated.index]
colnames(expression)[1] = 'Symbol'


# Option 2: vsn
#library(vsn)
#v = vsn2(as.matrix(2**expression[,-1]))
#data = exprs(v)
#multidensity(data)



keyTF = read.csv("/home/satoshi/new/perspective/neural_data/keyTFs_NSC.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
expression[expression[,'Symbol'] %in% keyTF[,1],]
expression[expression[,'Symbol'] %in% c('Ascl1','Pax6','Prox1','Hes1'),]

## 2. Take only TFs
## bioguo (http://www.bioguo.org/AnimalTFDB/download/gene_list_of_Mus_musculus.txt)
tfs = read.csv("/home/satoshi/new/perspective/bioguo_mouseTFs.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
I = which(expression[,'Symbol'] %in% tfs[,'mgi_symbol'])
exp.tf = expression[I,]
#exp.tf.variance = expression.variance[I,]

## Optional: Take only TFs present in the final network
#net = read.csv("/home/satoshi/new/perspective/esc/nuria/SCC1_combined_DEPCs_nuria_ESC_1-2-3_allCircuits.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#a = unique(c(net[,1],net[,3]))
#tfs = tfs[match(a, toupper(tfs[,1])),,drop=FALSE]
#I = which(expression[,'Symbol'] %in% tfs[,'mgi_symbol'])
#exp.tf = expression[I,]

## 3. Compute expression distance between all pairs within a sample
## Aim: showw if the Oct4-Sox2 ratio is significantly different between ESC and Ecto/Meso
## Ratios
ratio.outer = list()
for(i in 2:ncol(exp.tf)){
  m = outer(exp.tf[,i], exp.tf[,i], "-")
  ratio.outer[[i]] = m
}

## Take random combinations and compute ratio difference
# 1. NSC - Neuron
n.replicates = 4
com = rbind(
  sample(which(colnames(exp.tf)=='NSC'), n.replicates,replace=FALSE),
  sample(which(colnames(exp.tf)=='Neuron'), n.replicates,replace=FALSE)
  )
# 2. NSC - Astroglia
n.replicates = 4
com = rbind(
  sample(which(colnames(exp.tf)=='NSC'), n.replicates,replace=FALSE),
  sample(which(colnames(exp.tf)=='Astroglia'), n.replicates,replace=FALSE)
  )
# 3. Neuron - Astroglia


M = matrix(nrow = (nrow(ratio.outer[[2]])^2 /2 - nrow(ratio.outer[[2]])/2), ncol = n.replicates)
for(i in 1:ncol(com)){
  comb = com[,i]  
  ## Compute the ratio difference
  ratio.diff = ratio.outer[[comb[1]]] - ratio.outer[[comb[2]]]
  # Option 1: Devide the ratio difference by the ESC ratio
  ratio.diff = ratio.diff / ratio.outer[[comb[1]]]
  # Option 2: Devide the ratio difference by the ESC ratio
  #ratio.diff.esc_ecto = ratio.diff.esc_ecto / abs(outer(exp.tf[,'ESC'], exp.tf[,'ESC'], "/"))  
  #rownames(ratio.diff) = colnames(ratio.diff) = rownames(ratio.outer[[comb[1]]]) = colnames(ratio.outer[[comb[1]]]) = rownames(ratio.outer[[comb[2]]]) = colnames(ratio.outer[[comb[2]]]) = exp.tf[,'Symbol']
  M[,i] = ratio.diff[upper.tri(ratio.diff)]
}
## Get rownames
I = which(upper.tri(ratio.outer[[2]]),arr.ind=TRUE)
genes = sprintf("%s__%s", exp.tf[,'Symbol'][I[,1]], exp.tf[,'Symbol'][I[,2]]); rownames(M) = genes
# 1.
colnames(M) = rep('NSC_Neuron',n.replicates)
# 2.
colnames(M) = rep('NSC_Astroglia',n.replicates)
# 3.


## Convert infinite values into real number
M[M==Inf] = 10000
M[M==-Inf] = -10000

## Intensity binning
intensity = cbind(
  exp.tf[match(exp.tf[I[,1],'Symbol'], exp.tf[,1]),'NSC'],
  exp.tf[match(exp.tf[I[,2],'Symbol'], exp.tf[,1]),'NSC'],
  #exp.tf[match(exp.tf[I[,1],'Symbol'], exp.tf[,1]),'Neuron'],
  #exp.tf[match(exp.tf[I[,2],'Symbol'], exp.tf[,1]),'Neuron']
  exp.tf[match(exp.tf[I[,1], 'Symbol'], exp.tf[,1]),'Astroglia'],
  exp.tf[match(exp.tf[I[,2], 'Symbol'], exp.tf[,1]),'Astroglia']
  )
intensity = rowSums(intensity,na.rm=TRUE)
names(intensity) = genes

library(geneplotter)
multidensity(M,xlim=c(-10,10))
## Optional: MAD normalization?
for(i in 1:ncol(M)){
  M[,i] = (M[,i] - median(M[,i],na.rm=TRUE)) / mad(M[,i],na.rm=TRUE)
}
multidensity(M,xlim=c(-10,10))

## Simple limma test
K = 30
library(limma)
q = quantile(intensity, probs=seq(0,1,length.out=K+1))
I = cut(intensity, breaks=q,labels=FALSE)
I[is.na(I)] = 1
pval = rep(NA_real_,nrow(M))
padj = rep(NA_real_,nrow(M))
for (k in 1:K) {
  ## median lift-up
#  s = apply(D[I == k,2:6],1,sd,na.rm=TRUE)
#  m = apply(D[I == k,2:6],1,mean,na.rm=TRUE)
#  n = apply(is.finite(as.matrix(D[I == k,2:6])),1,sum,na.rm=TRUE)
#  S = median(s, na.rm=TRUE)
#  snew = s
#  snew[s < S] = S
#  t <- sqrt(n) * m / snew
#  p.value <- 2*pt(-abs(t),df=n-1)
#  padj[I == k] = p.adjust(p.value,method="BH")
  
  ## limma
  fit = eBayes(lmFit(M[I == k,]))
  pval[I == k] = fit$p.value
  padj[I == k] = p.adjust(fit$p.value,method="BH")
  print(sum(padj[I == k] <= 0.05,na.rm=TRUE))
}
names(padj) = rownames(M)
hist(padj); length(padj)
J = which(padj <= 0.05)
length(J)

## Check important combinations
keyComb = c('Ascl1__Pax6')
padj[which(names(padj) %in% keyComb)]
M[which(rownames(M) %in% keyComb),]

## Remove the ratio difference <0.5
ratio = cbind(padj,'mean.ratio.difference'=rowMeans(M))
# 1. Neuron
write.table(ratio, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandNeuron_GSE9566Jens.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)
# 2. Astroglia
write.table(ratio, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandAstroglia_GSE9566Jens.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)

## Load the files
# 1.
ratio = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandNeuron_GSE9566Jens.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 2. 
ratio = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandAstroglia_GSE9566Jens.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

# Option 1: padj and ratio
# 1. NSC-neuron
sig = ratio[ratio[,'padj']<=0.05 & abs(ratio[,'mean.ratio.difference'])>=0.5, ]
# 2. NSC-astroglia
sig = ratio[ratio[,'padj']<=0.05 & abs(ratio[,'mean.ratio.difference'])>=0.5, ]
# Option 2: only padj
#sig = ratio[ratio[,'padj']<=0.05, ]

sig[which(rownames(sig) %in% keyComb),]

## Volcano plot
plot(rowMeans(M), -log10(padj),col=ifelse(padj <= 0.05 & abs(rowMeans(M)) >=0.5, "red", "black"), pch=20, main="ESC vs Ecto",xlab="log2(mean ratio)", ylab="-log10(adjusted p-value)",cex.axis=1.3,cex.lab=1.3,cex=0.5, xlim=c(-10,10))

## Extract combinations existing in the network
#net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/SCC1_combined_SCCs_GSE49991_HSC_1-2-3.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#a = unique(c(net[,1],net[,3]))
#a = expand.grid(a,a)
#g = sprintf("%s__%s", a[,1], a[,2])
#sig.net = sig[match(g, toupper(rownames(sig))),,drop=FALSE]
#sig.net = sig.net[complete.cases(sig.net),]
#dim(sig.net)

## Separate genes
sig = sig[complete.cases(sig),]
g1.tf = unique(unlist(strsplit(rownames(sig),"__")))

## Export all the significant TFs
# 1. NSC-Neuron
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_RatioDifference_NSCandNeuron_GSE9566Jens.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
#sig = sig[complete.cases(sig),]
#write.table(sig, "/home/satoshi/new/perspective/neural_data/RatioDifference_NSCandNeuron_GSE9566Jens.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)
write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/symbols_RatioDifference_NSCandNeuron_GSE9566Jens_3.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
sig = sig[complete.cases(sig),]
write.table(sig, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandNeuron_GSE9566Jens_3.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)

# 2. NSC-Astroglia
#write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/symbols_RatioDifference_NSCandAstroglia_GSE9566Jens.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
#sig = sig[complete.cases(sig),]
#write.table(sig, "/home/satoshi/new/perspective/neural_data/RatioDifference_NSCandAstroglia_GSE9566Jens.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)
write.table(g1.tf, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/symbols_RatioDifference_NSCandAstroglia_GSE9566Jens_3.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
sig = sig[complete.cases(sig),]
write.table(sig, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandAstroglia_GSE9566Jens_3.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)



################################################################
## Take intersection of the above three differential analysis ##
##   and make a network 
################################################################
## Option 1: simple intersection
# 1. NSC-Neuron
#g1 = read.csv("/home/satoshi/new/perspective/neural_data/RatioDifference_NSCandNeuron_GSE9566Jens.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 2. NSC-Astroglia
#g2 = read.csv("/home/satoshi/new/perspective/neural_data/RatioDifference_NSCandAstroglia_GSE9566Jens.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 3. Erythroid-Myeloid (standard DE test)
#g3 = read.csv("/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p01.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#g3[g3[,1]=='Sfpi1',1]='Spi1'
#g3 = read.csv("/home/satoshi/new/perspective/hematopoietic_data/symbols_RatioDifference_ErythroidandMyeloid_GSE49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# Intersection
#a = intersect(intersect(g1[,1],g2[,1]),g3[,1])
#write.table(a, "/home/satoshi/new/perspective/hematopoietic_data/symbols_RatioDifference_combined_GSE49991.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Option 2: Take combinations whose genes are both present in g3
# 1. NSC-Neuron
#rd1 = read.csv("/home/satoshi/new/perspective/neural_data/RatioDifference_NSCandNeuron_GSE9566Jens.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote=""); g1 = rownames(rd1)
rd1 = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandNeuron_GSE9566Jens_3.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote=""); g1 = rownames(rd1)
#
# 2. NSC-Astroglia
#rd2 = read.csv("/home/satoshi/new/perspective/neural_data/RatioDifference_NSCandAstroglia_GSE9566Jens.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote=""); g2 = rownames(rd2)
rd2 = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/RatioDifference_NSCandAstroglia_GSE9566Jens_3.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote=""); g2 = rownames(rd2)
#
# 3. Neuron-Astroglia (standard DE test)
g3 = read.csv("/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
g3[g3[,1]=='Sfpi1',1]='Spi1'

## Export all intersection ratio pairs
g12 = intersect(g1,g2)
G12 = g12[sign(rd1[match(g12, rownames(rd1)),2] * rd2[match(g12, rownames(rd2)),2]) == -1]
T = cbind(rd1[rownames(rd1) %in% G12,], rd2[rownames(rd2) %in% G12,])
write.table(T, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/intersect_RatioDifference_NSCandAstroglia_GSE9566Jens_3.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

# Intersection
g12 = intersect(g1,g2)
# Take combinations whose genes both exist in g3 and the ratio
#   difference is the opposite between the two
a = do.call('rbind',lapply(strsplit(g12,"__"),function(x){x}))
a = a[which(a[,1] %in% g3[,1] & a[,2]  %in% g3[,1]),]
a = sprintf("%s__%s", a[,1], a[,2])
G12 = a[sign(rd1[match(a, rownames(rd1)),2] * rd2[match(a, rownames(rd2)),2]) == -1]
g12.tf = unique(unlist(strsplit(G12,"__")))
g123 = intersect(g12.tf,g3[,1])
g123 = c(g123,'Hes1')
write.table(g123, "/home/satoshi/new/perspective/neural_data/symbols_DEbetweenNeuronandAstroglia_RatioDifference__GSE9566_4metacore_padj0p05.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
#write.table(g123, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/symbols_DEbetweenNeuronandAstroglia_RatioDifference__GSE9566_4metacore_padj0p05_3.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
write.table(G12, "/home/satoshi/new/perspective/neural_data/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
#write.table(G12, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/used_combinations_RatioDifference_NSC_combined_GSE9566Jens_3.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Extract all interactions from transfac
#transfac = read.csv("/home/satoshi/new/ENCODE/transfac_experimental.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#a = transfac[which(transfac[,1] %in% toupper(g3[,1]) & transfac[,3] %in% toupper(g3[,1])),]
#write.table(a, "/home/satoshi/new/perspective/neural_data/transfac_symbols_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Make the phenotype file 
# make the matrix
g3 = g3[,1]
M = matrix(nrow=length(g3), ncol=3)
rownames(M) = g3; colnames(M) = c('NSC','Neuron','Astroglia')
## 1. Parental cell is all 1    
M[,'NSC'] = 1
## 2. Daughter cell types are differential expression
updown = read.csv("/home/satoshi/new/perspective/neural_data/updown_SCC__GSE9566_Neuron-Astroglia.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
updown[updown[,1]=='SFPI1',1]='SPI1'
a = updown[match(toupper(rownames(M)), updown[,1]),]
a = cbind(a[,1], ifelse(a[,2]=="UP",1,0), ifelse(a[,2]=="UP",0,1))
M[,2] = a[,2]; M[,3] = a[,3]
write.table(M, "/home/satoshi/new/perspective/neural_data/Phenotype_NSC_GSE9566_Neuron-Astroglia_g3padj0p01.txt",sep="\t",row.names=TRUE,quote=FALSE,col.names=FALSE)


## Get network from metacore ##
# 1. transfac TF-DNA binding experimental
#transfac = read.csv("/home/satoshi/new/perspective/neural_data/transfac_symbols_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#colnames(transfac) = c('Gene.Symbol.From','Effect','Gene.Symbol.To')

# 2. metacore
# Option 1: all DETFs
metacore.interaction = read.csv("/home/satoshi/new/perspective/neural_data/metacore_NSC_GSE9566_neuron_vs_astroglia_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/neural_data/metacore_NSC_GSE9566_neuron_vs_astroglia_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option2: ratio difference
metacore.interaction = read.csv("/home/satoshi/new/perspective/neural_data/metacore_NSC_GSE9566_ratioDifference_neuron_vs_astroglia_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/neural_data/metacore_NSC_GSE9566_ratioDifference_neuron_vs_astroglia_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 3.
#metacore.interaction = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/metacore_NSC_GSE9566_neuron_vs_astroglia_interaction_2.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
#metacore.object = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/metacore_NSC_GSE9566_neuron_vs_astroglia_object_2.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

# Remove undirected interactions and extract needed information
a = metacore.interaction[which(
  (metacore.interaction$Effect=='Activation'
   | metacore.interaction$Effect=='Inhibition'
   | metacore.interaction$Effect=='Unspecified')
  & (metacore.interaction$Mechanism=='Transcription regulation'
     | metacore.interaction$Mechanism=='Binding')),
  c('Network.Object..FROM.','Effect','Network.Object..TO.','Mechanism')]

# Map objects to gene names
a = cbind(a,'Gene.Symbol.From'=toupper(metacore.object[match(a[,'Network.Object..FROM.'],metacore.object[,'Network.Object.Name']),'Gene.Symbol']))
a = cbind(a,'Gene.Symbol.To'=toupper(metacore.object[match(a[,'Network.Object..TO.'],metacore.object[,'Network.Object.Name']),'Gene.Symbol']))
a[,'Effect']=tolower(a[,'Effect'])

# Combine the two sources (remove redundancy)
literature = a[,c('Gene.Symbol.From','Effect','Gene.Symbol.To')]
literature = cbind(as.character(literature[,1]), as.character(literature[,2]),as.character(literature[,3])); colnames(literature) = c('Gene.Symbol.From','Effect','Gene.Symbol.To')
literature = literature[literature[,1]!="" & literature[,3]!="",]
L = sprintf("%s__%s",literature[,1], literature[,3])
#T = sprintf("%s__%s",transfac[,1], transfac[,3])
#I = which(!T %in% L) # unique IDs
#b = rbind(literature, transfac[I,])

# Add self-loops
selfLoops = read.csv("/home/satoshi/common_files/metacore_selfLoops.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
a = unique(c(literature[,1], literature[,3]))
colnames(selfLoops) = colnames(literature)
literature = rbind(literature, selfLoops[selfLoops[,1] %in% a,])
ids = unique(c(as.character(literature[,1]), as.character(literature[,3])))

## Export a .sif file for cytoscape
write.table(literature, "/home/satoshi/new/perspective/neural_data/metacore_DEbetweenNeuronandAstroglia_GSE9566_ratioDifference_4metacore_padj0p05.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)
## Hey1, Olig2, Sox9, Sox8, Emx2, Cebpb, Lmo4, Nkx2-2, Pax6, Olig1, Sox2, Hes5, Nkx6-1, Gli3, Gli2, Ascl1

   ## Get SCC ##


## Subset the phenotype file only for the genes present in the network
# 1. metacore + transfac
#net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. metacore only
#net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. metacore only, ratioDifference
net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_metacore_DEbetweenNeuronandAstroglia_GSE9566_ratioDifference_4metacore_padj0p05.sif",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 4. metacore only 3
#net = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05_3.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

net[is.na(net)] = "NA"
ids = unique(c(net[,1],net[,3]))
phe = read.csv("/home/satoshi/new/perspective/neural_data/Phenotype_NSC_GSE9566_Neuron-Astroglia_g3padj0p01.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="",row.names=1)
rownames(phe) = toupper(rownames(phe))
p = phe[rownames(phe) %in% ids,]
write.table(p, "/home/satoshi/new/perspective/neural_data/BoolData_NSC_GSE9566_ratioDifference_Neuron-Astroglia_g3padj0p01.txt",sep="\t",row.names=TRUE,quote=FALSE,col.names=FALSE)
net = net[net[,1] %in% rownames(p),]
net = net[net[,3] %in% rownames(p),]
# 1. metacore + transfac
#write.table(net, "/home/satoshi/new/perspective/neural_data/SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 2. metacore
#write.table(net, "/home/satoshi/new/perspective/neural_data/SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 3.  metacore only, ratioDifference
write.table(net, "/home/satoshi/new/perspective/neural_data/SCC1_metacore_DEbetweenNeuronandAstroglia_GSE9566_ratioDifference_4metacore_padj0p05.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)
# 4.  metacore only 3
#write.table(net, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05_3.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)

# Make adjacency matrix
# 1. transfac
#net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. metacore only
#net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# metacore only + ratioDifference
net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_metacore_DEbetweenNeuronandAstroglia_GSE9566_ratioDifference_4metacore_padj0p05.sif",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
#net = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05_3.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_1st_SCC1_metacore_DEbetweenNeuronandAstroglia_GSE9566_ratioDifference_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#net = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_1stNSCC_newAdj/SCC1_1st_SCC1_metacore_DEbetweenNeuronandAstroglia_GSE9566_ratioDifference_4metacore_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
net = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7/SCC1_adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

enum = matrix(c(seq_along(unique(c(net[,1],net[,3]))), unique(c(net[,1],net[,3]))), ncol=2)
b = cbind(enum[match(net[,1],enum[,2]),1],enum[match(net[,3],enum[,2]),1]); class(b)='numeric'
length(unique(c(b[,1],b[,2]))) #unique node number 
b = cbind(b,net[,c(1,3)])
#write.table(b, "/home/satoshi/new/perspective/neural_data/enum_SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
#write.table(b, "/home/satoshi/new/perspective/neural_data/enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(b, "/home/satoshi/new/perspective/neural_data/enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
#write.table(b, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05_3.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
#write.table(b, "/home/satoshi/new/perspective/neural_data/ordered_mBSim2_1stNSCC_newAdj/enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(b, "/home/satoshi/new/perspective/neural_data/ordered_degree7/enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

num = length(unique(c(as.numeric(b[,1]), as.numeric(b[,2]))))
mat = matrix(0,num,num)
rownames(mat)=colnames(mat)=enum[,2]
# 1. Indices of activation
act = net[net[,2]=='activation',]
mat[cbind(match(act[,1],rownames(mat)),match(act[,3],colnames(mat)))]=1
# 2. Indices of inhibition
inh = net[net[,2]=='inhibition',]
mat[cbind(match(inh[,1],rownames(mat)),match(inh[,3],colnames(mat)))]=-1
# 3. Indices of unspecified
uns = net[tolower(net[,2])=='unspecified',]
#mat[cbind(match(uns[,1],rownames(mat)),match(uns[,3],colnames(mat)))]=2
#write.table(mat, "/home/satoshi/new/perspective/neural_data/adjacency_enum_SCC1_metacore_transfac_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
#write.table(mat, "/home/satoshi/new/perspective/neural_data/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
write.table(mat, "/home/satoshi/new/perspective/neural_data/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
#write.table(mat, "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_3/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05_3.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
#write.table(mat, "/home/satoshi/new/perspective/neural_data/ordered_mBSim2_1stNSCC_newAdj/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
write.table(mat, "/home/satoshi/new/perspective/neural_data/ordered_degree7/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)

# Resize Booldata
booldata = read.csv("/home/satoshi/new/perspective/neural_data/BoolData_NSC_GSE9566_ratioDifference_Neuron-Astroglia_g3padj0p01.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
p = booldata[booldata[,1] %in% rownames(mat),]
write.table(p, "/home/satoshi/new/perspective/neural_data/BoolData_NSC_GSE9566_ratioDifference_Neuron-Astroglia_g3padj0p01.txt",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Run Malai's tool ##


####################################################################
## Import the contextualized adjacency matrix and make a sif file ##
####################################################################
# 1.
# 2. GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
rowname = predicted.state[,1]
predicted.state = predicted.state[,-1]
a = cbind(rowMeans(predicted.state[,seq(1,ncol(predicted.state),2)]), rowMeans(predicted.state[,seq(2,ncol(predicted.state),2)]))
a[a>=0.5]=1
a[a<0.5]=0
rownames(a)=rowname
write.table(a, "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_averaged_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)

B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
b = rowMeans(B)>=0.5
b[b==TRUE]=1
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
ind_optim = which(G!=0)  # nonzero elements
ind_actinh = which(G==2) # 2 unspecified
G1 = G
G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
G1[ind_optim[!b[1:length(ind_optim)]]] = 0
pruned.mat = G1

#rownames(pruned.mat) = colnames(pruned.mat) = rownames(original.mat)
## Adjacency matrix to sif
# 1. acivation
I = which(pruned.mat==1,arr.ind=TRUE)
act = cbind(rownames(pruned.mat)[I[,1]], "activation",colnames(pruned.mat)[I[,2]])
# 2. inhibition
I = which(pruned.mat==-1,arr.ind=TRUE)
inh = cbind(rownames(pruned.mat)[I[,1]], "inhibition",colnames(pruned.mat)[I[,2]])

# 1.
# 2.
write.table(rbind(act,inh), "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_averaged_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)



#############################################################
## Map back the significant, ratio-difference combinations ##
#############################################################
# Option 1: 
#predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 2: 
#predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/used_combinations_RatioDifference_NSC_combined_GSE9566Jens_2.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 3:
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 4 (replicate of Option 3)
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/initialState_predicted_BoolData_NSC_GSE9566_ratioDifference_g3padj0p05.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 5 (newBoolSim)
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 6 (oredered contextualization)
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 7 (oredered inhibitionDominant)
#predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 8 (oredered contextualization fixed)
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 9 ordered_mBSim2_NSCfixed_RUNX2-NEUROD1
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 10 ordered_mBSim2_NSCfixed_attractor
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 11 ordered_mBSim2
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 12: NSC_ordered_1000_1stNSCC
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 13: ordered_mBSim2_newAdj
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 14: ordered_degree7_2000
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")


## Find seesaw pairs in the initial SCC ##
#G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
genes = rownames(G)
g = lapply(usedCombi[,1],function(x){
  a = strsplit(x,"__")
  if(sum(toupper(a[[1]]) %in% toupper(genes)) == 2){
    x
  }
})
names(g) = usedCombi[,1]
g = sapply(g, function(x){x[!sapply(x,is.null) ]})
g = g[!sapply(g, function(x){length(x)==0})]
length(g)
##

## Option1: For each combination, extract predicted states
#P = lapply(usedCombi[,1],function(x){
#  a = strsplit(x,"__")
#  b = predicted.state[match(toupper(a[[1]]), predicted.state[,1]),]
#  colnames(b) = c('Symbol','FDCP','Erythroid','Myeloid')
#  if(sum(is.na(b[,1]))==0){b}
#})
#names(P) = usedCombi[,1]
#P = P[!sapply(P, is.null)]
## Choose seesaw combinations
#p = lapply(P,function(x){
#  a = x[,-1]
#  if(sum(a[,1])==2){
#    if(sum(rowSums(a)==2)==2){
#      if(sum(a[,2])==1 && sum(a[,3])==1){
#        x
#     }
#    }
#  }
#})
#p = p[!sapply(p, is.null)]
# Save all the important files in 'result' directory

## Option 2: For multiple configurations
rownames(predicted.state) = predicted.state[,1]
predicted.state = predicted.state[,-1]
p = data.frame(t(predicted.state))
nConfiguration = ncol(predicted.state)/2
ind = gl(nConfiguration,2)
p = split(p, ind)
## For each combination, extract predicted states
P = lapply(usedCombi[,1],function(x){
  v = lapply(p,function(y){
    a = strsplit(x,"__")
    if(all(!is.na(match(toupper(a[[1]]),colnames(y))))){
      y[,match(toupper(a[[1]]),colnames(y))]
    }
  })
  v
})
names(P) = usedCombi[,1]
P = sapply(P, function(x){x[!sapply(x,is.null) ]})
P = P[!sapply(P, function(x){length(x)==0})]

# Option 1: 2 state opposite
p = lapply(P, function(y){
  v = lapply(y, function(x){
    a = t(x)
    if(sum(rowSums(a)==1)==2){
      if(sum(colSums(a)==1)==2){
        x
      }    
    }
  })
  v
})
# Option 2: 3 state seesaw
#p = lapply(P, function(y){
#  v = lapply(y, function(x){
#    a = t(x)
#    if(sum(a[,1])==2){
#      if(sum(rowSums(a)==2)==2){
#        if(sum(a[,2])==1 && sum(a[,3])==1){
#          x
#        }
#      }
#    }
#  })
#  v
#})
p = sapply(p, function(x){x[!sapply(x,is.null) ]})
p = p[!sapply(p, function(x){length(x)==0})]
# 1. NSC 1-usedCombi 1.
#save(P,p, nConfiguration, file='usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.Rdata')
# 2. NSC 1-usedCombi 2. (0.05, 0.5)
#save(P,p, nConfiguration, file='usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.Rdata')
# 3. NSC 2-usedCombi 1.
#save(P,p, nConfiguration, file='GSE9566_allDETFRatioDifference_initialState_2/usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.Rdata') 
# 4. NSC 2-usedCombi 2. (0.05, 0.5)
#save(P,p, nConfiguration, file='GSE9566_allDETFRatioDifference_initialState_2/usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01_3.Rdata') 
# 5. NSC GSE9566_ratioDifference_padj0p05_metacoreOnly. (0.05, 0.5)
save(P,p, nConfiguration, file='GSE9566_ratioDifference_padj0p05_metacoreOnly/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 6. (replicate)
save(P,p, nConfiguration, file='GSE9566_ratioDifference_padj0p05_metacoreOnly_2/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 7. newBoolSim
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 8. ordered contextualization
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 9. ordered inhibitionDominant
#save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 10. ordered contextualization fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 11. ordered_mBSim2_NSCfixed_RUNX2-NEUROD1
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 12. ordered_mBSim2_NSCfixed_attractor
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 13. ordered_mBSim2
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 14. NSC_ordered_1000_1stNSCC
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 15. ordered_mBSim2_newAdj
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 16. ordered_degree7_2000
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')


## Compute the probability of each combination
# 1.
#load(file='usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.Rdata')
# 2.
# 3. NSC 2-usedCombi 1.
#load(file='GSE9566_allDETFRatioDifference_initialState_2/usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.Rdata')
# 4. NSC 2-usedCombi 2. (0.05, 0.5)
#load(file='GSE9566_allDETFRatioDifference_initialState_2/usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01_3.Rdata')
# 5. NSC GSE9566_ratioDifference_padj0p05_metacoreOnly. (0.05, 0.5)
load(file='GSE9566_ratioDifference_padj0p05_metacoreOnly/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 6. (replicate)
load(file='GSE9566_ratioDifference_padj0p05_metacoreOnly_2/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 7. newBoolSim
load(file='GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 8. ordered contextualization
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 9. ordered inhibitionDominant
#load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 10. ordered contextualization fixed
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
# 11. ordered_mBSim2_NSCfixed_RUNX2-NEUROD1
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 12. ordered_mBSim2_NSCfixed_attractor
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 13. ordered_mBSim2
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 14. NSC_ordered_1000_1stNSCC
load(file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 15. ordered_mBSim2_newAdj
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
# 16. ordered_degree7_2000
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')

fraction = lapply(p, function(y){
  length(y)/nConfiguration*100
})
# Match with ratio-stats seesaw
names(fraction)[which(names(fraction) %in% usedCombi[,1])]
c('Esr1__Runx2','Lef1__Runx2','Esr1__Stat5a') %in% names(fraction)[which(names(fraction) %in% usedCombi[,1])] 


####################################################################
## Import the contextualized adjacency matrix and make a sif file ##
####################################################################
# 1.
#original.mat = read.csv("/home/satoshi/new/perspective/neural_data/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#pruned.mat = read.csv("/home/satoshi/new/perspective/neural_data/initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2.
#original.mat = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_4metacore_padj0p05_2.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#pruned.mat = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3.
original.mat = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
pruned.mat = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_averaged_contextualized_adjacency_SCC1_GSE9566_ratioDifference_padj0p05_metacoreOnly.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")

rownames(pruned.mat) = colnames(pruned.mat) = rownames(original.mat)
## Adjacency matrix to sif
# 1. acivation
I = which(pruned.mat==1,arr.ind=TRUE)
act = cbind(rownames(pruned.mat)[I[,1]], "activation",colnames(pruned.mat)[I[,2]])
# 2. inhibition
I = which(pruned.mat==-1,arr.ind=TRUE)
inh = cbind(rownames(pruned.mat)[I[,1]], "inhibition",colnames(pruned.mat)[I[,2]])

# 1.
#write.table(rbind(act,inh), "/home/satoshi/new/perspective/neural_data/initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 2.
#write.table(rbind(act,inh), "/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3.
write.table(rbind(act,inh), "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_averaged_contextualized_adjacency_SCC1_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)


####################################################
## Compute gene retroactivity of each combination ##
####################################################
# Option 1: NSC1 entire network
#net = read.csv("/home/satoshi/new/perspective/neural_data/initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 2: NSC1 SCC
#net = read.csv("/home/satoshi/new/perspective/neural_data/SCC1_initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 3: NSC2 entire network
# Option 4: NSC2 SCC1
#net = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_allDETFRatioDifference_initialState_2/SCC1_initialState_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566_interaction.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 5:  NSC GSE9566_ratioDifference_padj0p05_metacoreOnly. (0.05, 0.5)
net = read.csv("GSE9566_ratioDifference_padj0p05_metacoreOnly/SCC1_initialState_averaged_contextualized_adjacency_SCC1_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 6: replicate
net = read.csv("GSE9566_ratioDifference_padj0p05_metacoreOnly/SCC1_initialState_averaged_contextualized_adjacency_SCC1_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 7: GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed
net = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_averaged_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 

fraction = lapply(p, function(y){
  length(y)/nConfiguration*100
})
# Compute gene retroactivity 
genes = unique(c(net[,1],net[,3]))
retro.gene = lapply(genes,function(x){
  a=net[net[,1] %in% x | net[,3] %in% x,]
  cbind('activation'=sum(a[,2]=='activation'),'inhibition'=sum(a[,2]=='inhibition'),'both'=length(a[,2]))
})
names(retro.gene)=genes

# Compute retroactivity of each pair
retro.pairs = do.call('rbind',lapply(names(fraction),function(x){
  g = toupper(strsplit(x,"__")[[1]])
  m = match(g, names(retro.gene))
  if(sum(is.na(m))==0){
    y = retro.gene[match(g, names(retro.gene))]
    c(colSums(do.call('rbind',lapply(y,function(z){z}))), y[[1]], y[[2]])
  }else{
    c(rep(NA,9))
  }  
}))
rownames(retro.pairs) = names(fraction)
colnames(retro.pairs) = c('act.sum','inh.sum','both.sum','act.g1','inh.g1','both.g1','act.g2','inh.g2','both.gene2')

## Add fraction
retro.pairs = as.data.frame(cbind('fraction' = unlist(fraction[match(rownames(retro.pairs), names(fraction))]), retro.pairs))


## Add hierarchical information ##
# Load vertex-sort
source("/home/satoshi/programmes/Rs/perspective/vertexSort_4loading.R")
# Perform vertex sort
V = unique(c(net[,1],net[,3]))
E = sprintf("%s__%s__%s",net[,1], net[,2], net[,3])
M = as.data.frame(matrix(nrow=length(V), ncol=3))
colnames(M) = c('colour','discovery','finish')
rownames(M) = V
hierarchy = VERTEX_SORT(net, V, E)
a = do.call('rbind',lapply(strsplit(rownames(retro.pairs),"__"),function(x){toupper(x)}))
retro.pairs = cbind(retro.pairs, 'Layer.g1'=hierarchy[match(a[,1], names(hierarchy))], 'Layer.g2'=hierarchy[match(a[,2], names(hierarchy))])

## Order
# Remove incomplete cases
retro.pairs = retro.pairs[complete.cases(retro.pairs),]
retro.pairs[with(retro.pairs, order(-fraction, -both.sum)), ]
retro.pairs[with(retro.pairs, order(-both.sum)), ]
# Sall1, Tal1, Dbp, Klf4, Ebf1, Nfe2l2

## Check the ratioDifference
ratio = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/intersect_RatioDifference_NSCandAstroglia_GSE9566Jens_3.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
a = ratio[rownames(ratio) %in% rownames(retro.pairs) ,]
# Increase stringency
r = a[a[,'padj']<=0.05 & abs(a[,'mean.ratio.difference'])>=1 & a[,'padj.1']<=0.05 & abs(a[,'mean.ratio.difference.1'])>=1,]
retro.pairs.sig = retro.pairs[rownames(retro.pairs) %in% rownames(r),]
retro.pairs.sig[with(retro.pairs.sig, order(-both.sum)), ]

## Check if the genes in pairs are directly connected
v = do.call('rbind',lapply(strsplit(rownames(retro.pairs.sig),"__"),function(x){
  if(sum(net[,1] %in% toupper(x[1]) & net[,3] %in% toupper(x[2])) > 0
     | sum(net[,3] %in% toupper(x[1]) & net[,1] %in% toupper(x[2])) > 0
   ){
    1
  }else{
    0
  }
}))
retro.pairs.sig = cbind(retro.pairs.sig,'Direct.connection'=v)
retro.pairs.sig[with(retro.pairs.sig, order(-both.sum)), ]


## Export
write.table(retro.pairs, "/home/satoshi/new/perspective/neural_data/retroactivity_averaged_contextualized_adjacency_SCC1_metacore_RatioDifference_NSC_combined_GSE9566Jens_interaction.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=TRUE)



#################################################################
## Load all best configurations
# 1. (0.05, 0.5)
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. replicate (0.05, 0.5)
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. newBoolSim (0.05, 0.5)
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 4. orderedContextualization
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 5. orderedInhibitionDominant
#G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#G = as.matrix(G)
#B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 6. GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 7. ordered_mBSim2_NSCfixed_RUNX2-NEUROD1
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 8. ordered_mBSim2_NSCfixed_attractor
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 9. ordered_mBSim2
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 10. NSC_ordered_1000_1stNSCC
G = read.csv("/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 11. ordered_mBSim2_newAdj
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 12. ordered_degree7_2000
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")


ind_optim = which(G!=0)  # nonzero elements
ind_actinh = which(G==2) # 2 unspecified

scc = list()
for (i in 1:ncol(B)){
  b = B[,i]
  G1 = G
  G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
  G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
  G1[ind_optim[!b[1:length(ind_optim)]]] = 0

  # 1. acivation
  I = which(G1==1,arr.ind=TRUE)
  act = cbind(rownames(G1)[I[,1]], "->",colnames(G1)[I[,2]])
  # 2. inhibition
  I = which(G1==-1,arr.ind=TRUE)
  inh = cbind(rownames(G1)[I[,1]], "-|",colnames(G1)[I[,2]])

  write.table(rbind(act,inh), "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/SCC_temp/scc.sif",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
  system('perl /home/satoshi/programmes/perl/isaac_SCC.pl -f /home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/SCC_temp/scc.sif')
  g = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/SCC_temp/scc.sif.scc",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
  name = paste('conf',i,sep="")
  g = g[-1,]
  scc[[i]] = g
}
# 1. 
save(scc, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/all_scc.Rdata')
# 2.(replicate)
save(scc, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/all_scc.Rdata')
# 3.(replicate)
save(scc, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/all_scc.Rdata')
# 4. orderedContextualization
save(scc, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/all_scc.Rdata')
# 5. orderedInhibitionDominant
#save(scc, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/all_scc.Rdata')
# 6. orderedContextualization_fixed
save(scc, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/all_scc.Rdata')
# 7. ordered_mBSim2_NSCfixed_RUNX2-NEUROD1
save(scc, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/all_scc.Rdata')
# 8. ordered_mBSim2_NSCfixed_attractor
save(scc, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/all_scc.Rdata')
# 9. ordered_mBSim2
save(scc, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/all_scc.Rdata')
# 10. NSC_ordered_1000_1stNSCC
save(scc, file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/all_scc.Rdata')
# 11. ordered_mBSim2_newAdj
save(scc, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/all_scc.Rdata')
# 12. ordered_degree7_2000
save(scc, file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/all_scc.Rdata')

## For each SCC, check the occurrence of seesaw pairs in each SCC
# 1. GSE9566_ratioDifference_padj0p05_metacoreOnly
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/all_scc.Rdata')
# 2. GSE9566_ratioDifference_padj0p05_metacoreOnly2 
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/all_scc.Rdata')
# 3. GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/all_scc.Rdata')
# 4. orderedContextualization
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/all_scc.Rdata')
# 5. orderedInhibitionDominant
#load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
#usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/all_scc.Rdata')
# 5. orderedContextualization_fixed
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/all_scc.Rdata')
# 6. ordered_mBSim2_NSCfixed_RUNX2-NEUROD1
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/all_scc.Rdata')
# 7. ordered_mBSim2_NSCfixed_attractor
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/all_scc.Rdata')
# 8. ordered_mBSim2
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/all_scc.Rdata')
# 9. NSC_ordered_1000_1stNSCC
load(file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/all_scc.Rdata')
# 10. ordered_mBSim2_newAdj
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/all_scc.Rdata')
# 12. ordered_degree7_2000
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/usedCombi_initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.Rdata')
usedCombi = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/used_combinations_RatioDifference_NSC_combined_GSE9566Jens.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/all_scc.Rdata')


fraction = lapply(p, function(y){
  length(y)/nConfiguration*100
})
# Match with ratio-stats seesaw
seesaw = toupper(names(fraction)[which(names(fraction) %in% usedCombi[,1])])

# Check if both genes are present in each SCC
scc.fraction = lapply(seesaw, function(x){
  s = strsplit(x,"__")
  count = 0
  count.direct.connection = 0
  for(i in 1:length(scc)){
    genes = unique(c(scc[[i]][,1], scc[[i]][,3]))
    if(sum(s[[1]] %in% genes)==2){
      count = count+1
    }
    if(sum(scc[[i]][,1] %in% s[[1]][1] & scc[[i]][,3] %in% s[[1]][2]) > 0
       | sum(scc[[i]][,3] %in% s[[1]][1] & scc[[i]][,1] %in% s[[1]][2]) > 0
       ){
      count.direct.connection = count.direct.connection + 1
    }
  }    
  c(count / length(scc)*100, count.direct.connection / length(scc)*100)
})
names(scc.fraction) = seesaw

## Combine seesaw fraction and SCC fraction
scc.pairs = cbind(do.call('rbind',lapply(fraction,function(x){x})), do.call('rbind',lapply(scc.fraction,function(x){x})))
colnames(scc.pairs) = c('seesaw.state.fraction','scc.fraction','direct.connection')
scc.pairs[rev(order(scc.pairs[,3])),]

## Check the ratioDifference
ratio = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/intersect_RatioDifference_NSCandAstroglia_GSE9566Jens_3.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
a = ratio[rownames(ratio) %in% rownames(scc.pairs) ,]
scc.pairs = cbind(scc.pairs, ratio[match( rownames(scc.pairs), rownames(ratio)),])
scc.pairs[rev(order(scc.pairs[,2])),]

# 1.
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/scc_pairs.Rdata')
# 2.
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/scc_pairs.Rdata')
# 3. 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/scc_pairs.Rdata')
# 4. 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/scc_pairs.Rdata')
# 5. 
#save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/scc_pairs.Rdata')
# 6. 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/scc_pairs.Rdata')
# 7. 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/scc_pairs.Rdata')
# 8. 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/scc_pairs.Rdata')
# 9. 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/scc_pairs.Rdata')
# 10 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/scc_pairs.Rdata')
# 11 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/scc_pairs.Rdata')
# 12 
save(scc.pairs, file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/scc_pairs.Rdata')


## Reload
# 1.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/scc_pairs.Rdata')
# 2.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/scc_pairs.Rdata')
# 3.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_newBoolSim/scc_pairs.Rdata')
# 4.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/scc_pairs.Rdata')
# 5.
#load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedInhibitionDominant/scc_pairs.Rdata')
# 6.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/scc_pairs.Rdata')
# 7.
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_RUNX2-NEUROD1/scc_pairs.Rdata')
# 8.
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_NSCfixed_attractor/scc_pairs.Rdata')
# 9.
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2/scc_pairs.Rdata')
# 10
load(file='/home/satoshi/new/perspective/neural_data/NSC_ordered_1000_1stNSCC/scc_pairs.Rdata')
# 11
load(file='/home/satoshi/new/perspective/neural_data/ordered_mBSim2_newAdj/scc_pairs.Rdata')
# 12. ordered_degree7_2000
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/scc_pairs.Rdata')

# Increase stringency
scc.pairs.sig = scc.pairs[
  scc.pairs[,'padj']<=0.05 & abs(scc.pairs[,'mean.ratio.difference'])>=1 &
  scc.pairs[,'padj.1']<=0.05 & abs(scc.pairs[,'mean.ratio.difference.1'])>=1 &
  scc.pairs[,'seesaw.state.fraction']>=90 &
  scc.pairs[,'scc.fraction']>=90
  ,]
scc.pairs.sig[rev(order(scc.pairs.sig[,3])),]
#scc.pairs[rev(order(scc.pairs[,2])),]
#scc.pairs[rev(order(scc.pairs[,3])),]


rownames(scc.pairs)[grep('Lef1',rownames(scc.pairs), ignore.case=TRUE)]

## 
T = read.csv("GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_averaged_contextualized_adjacency_SCC1_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
T[,2] = ifelse(T[,2]=='activation','->', '-|')
T = T[!duplicated(T),]
write.table(T, "/home/satoshi/new/perspective/neural_data/arrow.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)


#########################################################
## Convert SCCs into adjacency matrix for DEPC finding ##
#########################################################
# 1.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/all_scc.Rdata')
# 2.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/all_scc.Rdata')
# 3.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/all_scc.Rdata')
# 4.
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/all_scc.Rdata')
# 5. ordered_degree7_2000
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/all_scc.Rdata')


## Convert each SCC into an adjacency matrix
# 1.
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. orderedContextualization
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. orderedContextualization_fixed
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 4. ordered_degree7_2000
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")


ind_optim = which(G!=0)  # nonzero elements
ind_actinh = which(G==2) # 2 unspecified

# 1. convert each configuration into an adjacency matrix
# 2. put 0 to non-SCC part in the adjacency matrix 
# 3. convert the adjacency matrix into the vector representation
M = matrix(nrow=length(G[G!=0]), ncol=ncol(B))
I.G = which(G!=0,arr.ind=TRUE)
rownames(M) = sprintf("%d__%d",I.G[,1],I.G[,2])
for (i in 1:ncol(B)){
#M = apply(B,2,function(x){
  ## Convert each configuration into an adjacency matrix
  b = B[,i]
  I.G = which(G!=0,arr.ind=TRUE)
  I.G = cbind(I.G,'interact'=0)

  ## Contextualization
  G1 = G  
  G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
  G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
  G1[ind_optim[!b[1:length(ind_optim)]]] = 0
  I.G[,3] = G1[I.G[,1:2]]
  
  ## Put 0 to non-SCC part in the adjacency matrix 
  s = scc[[i]]

  ## Find array indices of the SCC and maintain only those array indices and put 0 to all the others
  I = cbind(match(s[,1], rownames(G1)), match(s[,3], colnames(G1)))
  a1 = sprintf("%d__%d",I.G[,1],I.G[,2])
  a2 = sprintf("%d__%d",I[,1],I[,2])
  rownames(I.G) = a1
  I.G[which(!a1 %in% a2),3] = 0 # edges not present in the SCC
  I.G[,3]
  M[,i] = I.G[match(rownames(M), rownames(I.G)),3]
}

## Export for the matlab "doPerturbation.m"
# 1.
write.table( which(G!=0), "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 2. orderedContextualization
write.table( which(G!=0), "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3. orderedContextualization_fixed
write.table( which(G!=0), "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 4. ordered_degree7_2000
write.table( which(G!=0), "/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)


################################################################
## Check the frequency of DEPCs after 'runFindAllDEPCs_NSC.m' ##
################################################################
## load all SCCs
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/all_scc.Rdata')
scc = lapply(scc,function(x){
  x[x[,2]=='->',2] = 'activation'
  x[x[,2]=='-|',2] = 'inhibition'
  x
})
s.uniq = scc[!duplicated(scc)]

## Load DEPC file
depcs = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/vector_adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
l = apply(depcs,2,function(x){ paste(x,sep='',collapse='')})
length(table(l))

# Subset for specific genes
p = "RUNX2__LEF1"
#I1 = c(which(depcs[rownames(depcs) %in% p,]!=0))
p = "RUNX2__ESR1"
I2 = which(depcs[rownames(depcs) %in% p,]!=0)
p = "ESR1__STAT5A"
I3 = which(depcs[rownames(depcs) %in% p,]!=0)
p = "HEY1__MEF2C"
I4 = which(depcs[rownames(depcs) %in% p,]!=0)

# Option 1: more than one pair
I = NULL
I = intersect(I2, I3)
I = intersect(intersect(I2, I3), I4)
sub = depcs[,I]

# Option 2: only one pair
sub = depcs[,which(depcs[rownames(depcs)==p,]!=0)]


l = apply(sub,2,function(x){ paste(x,sep='',collapse='')})
b = split(l,l)  # binary -- file names
rep.file = do.call('rbind',lapply(b,function(x){names(x[1])}))

## Find the minimum DEPC
freq <- do.call('rbind',lapply(l, function(z){ sapply(gregexpr("0",z),function(y)if(y[[1]]!=-1) length(y) else 0)}))
freq = freq[rev(order(freq[,1])),] #descending

# match freq and rep.file
A = cbind(rep.file,'size'=freq[match(rep.file, names(freq))])
A = A[rev(order(A[,2])),]
b = A[1:100,1]
names(b)=NULL

# for each motif, compute the frequency
F = lapply(b,function(y){
  #a = names(x[1])
  # read in the file
  infile = outfile = paste("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/", y,".sif",sep="")
  motif = read.csv(infile,sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
  v = lapply(s.uniq,function(x){
    g = unique(c(motif[,1], motif[,3]))
    sub = x[x[,1] %in% g & x[,3] %in% g,]
    a = sprintf("%s__%s__%s",sub[,1],sub[,2],sub[,3])
    m = sprintf("%s__%s__%s",motif[,1],motif[,2],motif[,3])
    all(m %in% a) & all(a %in% m)
  })
  per = sum(v==TRUE)/length(v)

  n.s = length(unique(c(motif[,1],motif[,3])))
  e.s = nrow(motif)
  genes = paste(unique(c(motif[,1],motif[,3])),collapse=";")
  # return the number of nodes and interactions, and frequency
  c(n.s, e.s, per*100, genes)  
})
names(F) = b


p = "RUNX2__ESR1__STAT5A__HEY1__MEF2C"
save(F, file=paste("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/minimumDEPCs_",p,".Rdata",sep=""))

## Reload
# 1. Esr1-Runx2
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/minimumDEPCs_RUNX2__ESR1.Rdata')
# 2. ESR1__STAT5A
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/minimumDEPCs_ESR1__STAT5A.Rdata')
# 3. HEY1__MEF2
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/minimumDEPCs_HEY1__MEF2C.Rdata')
# 4. RUNX2__ESR1__STAT5A__HEY1__MEF2C
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/minimumDEPCs_RUNX2__ESR1__STAT5A__HEY1__MEF2C.Rdata')


T = do.call('rbind',lapply(F,function(x){x}))
head(T[rev(order(as.numeric(T[,3]))),])
head(T[order(as.numeric(T[,1])),])

## Find motifs with each node at least with one activation input  
# For each motif, check if all rows have at least one "1"
#for (i in 1:nrow(T)){
v = do.call('rbind',lapply(rownames(T),function(x){
  infile = paste("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/", x,sep="")
  #print(infile)
  motif = read.csv(infile,sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
  motif = motif[,-ncol(motif)]
  if( sum(colSums(motif==1)==0) ==0 ){
    'Yes'
  } else{
    'No'
  }
}))
rownames(v) = rownames(T)
T = cbind(T,v)
head(T[rev(order(as.numeric(T[,3]))),],10)



########################################################
##  Find motifs in all the SCCs and compute fractions ##
########################################################
## Bitoggle switch
#motif = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/sub_DEPCs/adjacency_DEPC_298627.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 1. ESR1-RUNX2
motif = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/adjacency_DEPC_924933.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#% small ones
motif = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/adjacency_DEPC_936776.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
motif = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/adjacency_DEPC_901727.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
motif = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/DEPCs/adjacency_DEPC_901729.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")



# 2. ESR1-STAT5A
motif = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_DEPC_99405.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# HEY1__MEF2C
motif = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_DEPC_182958.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Self-loops
motif = t(as.matrix(c('RUNX2','activation','RUNX2')))
#motif = t(as.matrix(c('GATA2','inhibition','GATA2')))

load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/all_scc.Rdata')
scc = lapply(scc,function(x){
  x[x[,2]=='->',2] = 'activation'
  x[x[,2]=='-|',2] = 'inhibition'
  x
})
s.uniq = scc[!duplicated(scc)]

v = lapply(s.uniq,function(x){
  g = unique(c(motif[,1], motif[,3]))
  sub = x[x[,1] %in% g & x[,3] %in% g,]
  a = sprintf("%s__%s__%s",sub[,1],sub[,2],sub[,3])
  m = sprintf("%s__%s__%s",motif[,1],motif[,2],motif[,3])
  all(m %in% a) & all(a %in% m)
})
sum(v==TRUE)/length(v)

#############################################
## Export sif files with different cutoffs ##
#############################################
# 1.
#G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#G = as.matrix(G)
#B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. GSE9566_ratioDifference_padj0p05_metacoreOnly_2
#G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#G = as.matrix(G)
#B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 4. ordered_degree7_2000
G = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/initialState_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

cutoff = c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95)
for (i in 1:length(cutoff)){
  # Decide the cutoff  
  x = cutoff[i]

  # 1. mean
  b = rowMeans(B)
  # 2. single best one
  #b = B[,100]
  
  b[b <= x]=0 
  ind_data = nrow(G)
  ind_optim = which(G!=0)  # nonzero elements
  ind_actinh = which(G==2); # 2 unspecified 
  G1 = G
  G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
  G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
  G1[ind_optim[!b[1:length(ind_optim)]]] = 0
  ## Export adjacency
  out = paste("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/adjacency_bestConfiguration_",x,".txt", sep="")
  write.table(G1, out ,sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
  
  ## Adjacency matrix to sif
  # 1. acivation
  I = which(G1==1,arr.ind=TRUE)
  act = cbind(rownames(G1)[I[,1]], "activation",colnames(G1)[I[,2]])
  # 2. inhibition
  I = which(G1==-1,arr.ind=TRUE)
  inh = cbind(rownames(G1)[I[,1]], "inhibition",colnames(G1)[I[,2]])
  out = paste("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/bestConfiguration_",x,".sif", sep="")
  write.table(rbind(act,inh), out ,sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
}

## Computed attractors of best configurations
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/all_scc.Rdata')
predicted.state = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization/initialState_predicted_BoolData_GSE9566_ratioDifference_padj0p05_metacoreOnly.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")





########################################################
## Check degrees of each node in the original network ##
########################################################
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
degree = do.call('rbind',lapply(rownames(G),function(x){
  sum(G[rownames(G) %in% x,]!=0) + sum(G[,colnames(G) %in% x]!=0)
}))
rownames(degree) = rownames(G)
degree[rev(order(degree)),]


##############################################
## Check the frequency of positive circuits ##
##############################################

## Run 'findAllCircuits.m' in Matlab ##

## 2. replicate (0.05, 0.5)
G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
I.edges = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/Pcircuits_in_all_SCCs_NSC_edgeIndices.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
I.edges = as.matrix(I.edges)
I.nodes = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/Pcircuits_in_all_SCCs_NSC_nodeIndices.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
I.nodes = as.matrix(I.nodes)
mode.of.action = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/Pcircuits_in_all_SCCs_NSC_modeOfAction.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
mode.of.action = as.matrix(mode.of.action)
mode.of.action[!is.finite(mode.of.action)]=0

## Convert node indices into names
genes = rownames(G)
I.nodes[I.nodes==0]=NA
I.genes = apply(I.nodes,2,function(x){
  genes[x]
})
I.genes[is.na(I.genes)]=0

## Make a unique PC identifier
M = cbind(I.genes, I.edges, mode.of.action)
ID = apply(M,1,function(x){
  paste(x,collapse="_")
})
ID.freq = table(ID)
ID.freq[order(ID.freq)]
length(ID)
length(ID.freq)

## Find the most frequent circuits with candidate seesaw pairs
scc.pairs.sig[rev(order(scc.pairs.sig[,3])),]
keyP = c('LEF1', 'RUNX2')
ID.freq = as.data.frame(ID.freq)
cir = do.call("rbind",apply(ID.freq,1,function(x){
  a = strsplit(x[1],"_")
  if(all(keyP %in% a[[1]])){
    x
  }
}))
cir[order(as.numeric(cir[,2])),]

## Most frequent circuit of all
ID.freq[ID.freq[,2]==max(ID.freq[,2]),]

## Extract the network of defined genes from each SCC
# Indices of non-0 elements
I_G = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/pertubation_nonzeroIndices.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
# SCC vector formats
V = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/pertubation_adjacencyVectors.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
keyG = c('LEF1', 'RUNX2','ESR1','SNAI2')
keyPCs = list()
for (i in 1:ncol(V)){
  ## Make adjacency matrix
  b = V[,i]
  G1 = G
  G1[I_G[,1]]=b

  ## extract the genes of interest
  pc = G1[match(keyG,rownames(G1)), match(keyG,colnames(G1))]  
  if( (sum(pc==-1) %% 2) ==0 ){
    keyPCs[[i]] = pc
  }
}
## Remove the NULL elements and 
keyPCs = keyPCs[!sapply(keyPCs, is.null)]
v.keyPCs = do.call('rbind',lapply(keyPCs,function(x){
  a = as.vector(x)
  a = paste(a,collapse="_")
}))
b = table(v.keyPCs)
f.keyPCs = lapply(names(b),function(x){
  m = matrix(as.numeric(strsplit(x,"_")[[1]]),ncol=4)
  rownames(m) = colnames(m) = keyG
  m
})

## Convert each unique PC into a sif file
for (i in 1:length(f.keyPCs)){
  mat = f.keyPCs[[i]]  
  if(!is.null(mat)){    
    # 1. acivation
    I = which(mat==1,arr.ind=TRUE)
    act = cbind(rownames(mat)[I[,1]], "activation",colnames(mat)[I[,2]])
    # 2. inhibition
    I = which(mat==-1,arr.ind=TRUE)
    inh = cbind(rownames(mat)[I[,1]], "inhibition",colnames(mat)[I[,2]])
    out = paste("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_2/uniqueKeyPC_",i,"_",b[i],".sif", sep="")
    write.table(rbind(act,inh), out ,sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
  }
}

#########################################
## Check the number of out-going edges ##
#########################################
# 1. ordered_0p00005_TFonly_500_mBSim2_ESCfixed
load(file='/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/scc_pairs.Rdata')
net = read.csv("/home/satoshi/new/perspective/neural_data/GSE9566_ratioDifference_padj0p05_metacoreOnly_orderedContextualization_fixed/initialState_averaged_predicted_contextualized_best_adjacency_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. ordered_degree7_2000
load(file='/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/scc_pairs.Rdata')
net = read.csv("/home/satoshi/new/perspective/neural_data/ordered_degree7_2000/bestConfiguration_0.5.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

scc.pairs[rev(order(scc.pairs[,3])),]


# Increase stringency
scc.pairs.sig = scc.pairs[
  scc.pairs[,'padj']<=0.05 & abs(scc.pairs[,'mean.ratio.difference'])>=1 &
  scc.pairs[,'padj.1']<=0.05 & abs(scc.pairs[,'mean.ratio.difference.1'])>=1 &
  scc.pairs[,'seesaw.state.fraction']>=90 &
  scc.pairs[,'scc.fraction']>=90 
  &  scc.pairs[,'direct.connection']>=50
  ,]
scc.pairs.sig[rev(order(scc.pairs.sig[,1])),]
scc.pairs.sig[rev(order(scc.pairs.sig[,3])),]
g = rownames(scc.pairs.sig[rev(order(scc.pairs.sig[,3])),])

d = lapply(g,function(x){
  s = strsplit(x,"__")
  min(c(sum(net[,1] %in% toupper(s[[1]][2])), sum(net[,1] %in% toupper(s[[1]][2]))))
})
names(d) = g


########################################### 
## Reduce the profile SCC size by degree ##
###########################################
G = read.csv("/home/satoshi/new/perspective/neural_data/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
d = do.call('rbind',lapply(rownames(G),function(x){
  s = sum(G[which(rownames(G) %in% x),]!=0) + sum(G[,which(rownames(G) %in% x)]!=0) 
  if(G[which(rownames(G) %in% x),which(rownames(G) %in% x)] !=0){
    s = s-1
  }
  s
}))
rownames(d) = rownames(G)
write.table(G[which(d>7),which(d>7)], "/home/satoshi/new/perspective/neural_data/ordered_degree7/adjacency_enum_SCC1_metacore_DEbetweenNeuronandAstroglia__GSE9566_ratioDifference_4metacore_padj0p05.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)


########################################### 
## Reduce the profile SCC size by degree ##
###########################################
expression = read.csv("/home/satoshi/new/perspective/neural_data/quantile_jensNSC_GSE9566Neuron-Astroglia.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
expression[expression[,'RowName']=='Sfpi1','RowName']='Spi1'
RowName = expression[,1]
#expression = expression[,-1]
library(limma)
expression[,-1] = normalizeQuantiles(expression[,-1])
colnames(expression) = gsub("\\.\\d+$","",colnames(expression),ignore.case=TRUE,perl=TRUE)

## Aggregate genes by highest variance across samples
variance = apply((expression[,which(colnames(expression) %in% c('NSC','Neuron','Astroglia'))]),1,var,na.rm=TRUE); names(variance)=RowName
variance.aggregated.index = tapply(seq_along(variance), names(variance), function(x){
  m = max(variance[x],na.rm=TRUE)
  I = which(variance[x]==m)
  x[I[1]]
})
## Use this index for padj and fold
expression = expression[variance.aggregated.index,]
rownames(expression) = RowName[variance.aggregated.index]
colnames(expression)[1] = 'Symbol'
expression = expression[,-1]
genes = c('Esr1','Runx2','Stat5a','Mef2c','Hey1')
e = expression[rownames(expression) %in% genes,]
e = cbind(rowMeans(e[,17:20]),rowMeans(e[,c(1:6,14)]),rowMeans(e[,c(7:13,15,16)]))












#########################################################################
#############################################
## (Not in use) TF target for perturbation ##
#############################################
## 1. MEF2C
metacore.interaction = read.csv("/home/satoshi/new/perspective/compound_perturbation/metacore_MEF2C_oneUpstream_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/compound_perturbation/metacore_MEF2C_oneUpstream_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")



# Remove undirected interactions and extract needed information
a = metacore.interaction[
  which
  (metacore.interaction$Mechanism!='Transcription regulation'
   & metacore.interaction$Mechanism!=""
   & metacore.interaction$Mechanism!="miRNA binding"
  # | metacore.interaction$Effect=='Inhibition'
  # | metacore.interaction$Effect=='Unspecified')
  #& (metacore.interaction$Mechanism=='Transcription regulation'
  #   | metacore.interaction$Mechanism=='Binding'
  #   | metacore.interaction$Mechanism=='Influence on expression'
  #))
  ),  
  c('Network.Object..FROM.','Effect','Network.Object..TO.','Mechanism')]

### Optional: Use this part only for 1st depmr ##
M = NULL
for(i in 1:nrow(a)){
  x = a[i,]
  n = a[i,'Network.Object..FROM.']
  if( length(unique(metacore.object[metacore.object[,'Network.Object.Name'] %in% n,'Gene.Symbol'])) >0 ) {
    M = rbind(M, cbind(x, "Gene.Symbol.From"=unique(metacore.object[metacore.object[,'Network.Object.Name'] %in% n,'Gene.Symbol'])))
  }
}
N = NULL
for(i in 1:nrow(M)){
  x = M[i,]
  n = M[i,'Network.Object..TO.']
  if( length(unique(metacore.object[metacore.object[,'Network.Object.Name'] %in% n,'Gene.Symbol'])) >0 ) {
    N = rbind(N, cbind(x, "Gene.Symbol.To"=unique(metacore.object[metacore.object[,'Network.Object.Name'] %in% n,'Gene.Symbol'])))
  }
}

y = cbind(toupper(as.character(N[,5])), N[,2], toupper(as.character(N[,6])), N[,4])
y = y[y[,1]!="" & y[,3]!="",]
head(y)
dim(y)

## 1. MEF2C
write.table(y, "/home/satoshi/new/perspective/compound_perturbation/metacore_oneUpstream_MEF2C_signalling.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)










