####################
### HSC system 2 ###
####################
## GSE49991-GPL7202 ##
setwd("/home/satoshi/new/perspective/hematopoietic_data/GSE49991")
library(oligo)
b = read.csv("GSE49991-GPL7202_series_matrix.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
Rows = b[,1]
b = b[,-1]
b = data.matrix(b)

## Normalization
library(geneplotter)
multidensity(b)
M = b
rownames(M) = Rows

## Get gene names from biomart by using 'Affymetrix Microarray moex 1 0 st v1 probeset ID(s)' ##
id_table=read.csv("GSE49991_family.soft",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
M = cbind('Gene.Symbol'=id_table[match(rownames(M),id_table[,"ID"]),'GENE_SYMBOL'], M)
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/data_GSE49991.csv",sep="\t",row.names=TRUE,quote=FALSE)

################
## Limma test ##
################
library(limma)
library(geneplotter)
data = read.csv("/home/satoshi/new/perspective/hematopoietic_data/data_GSE49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
data = data[complete.cases(data),]
sample_table = read.csv("/home/satoshi/new/perspective/hematopoietic_data/sample_table_hematopoietic.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
colnames(data) = sample_table[match(colnames(data),sample_table[,1]),2]
RowName = data[,1]
# 1. Only end points
data = data[,colnames(data) %in% c('FDCP','Erythroid_168h','Myeloid_168h')]

## Checking 3 genes
data = data[,colnames(data) %in% c('FDCP','Erythroid_120h','Myeloid_120h','Erythroid_168h','Myeloid_168h')]
data[match(c('Gata1','Sfpi1','Gata2'), RowName),]
##

colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)

library(vsn)
v = vsn2(as.matrix(2**data))
data = exprs(v)
multidensity(data)
# Option 2: quantile
#data = normalizeQuantiles(data)
#multidensity(data)

## Design matrix
library(limma)
f = sample_table[match(colnames(data),sample_table[,2]),2]
colnames(data) = gsub("_.+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
f = gsub("_.+$","",f,ignore.case=TRUE,perl=TRUE)
f <- factor(f, levels=unique(f))
design <- model.matrix(~0+f)
colnames(design) <- levels(f)
fit <- lmFit(data, design)
colSums(design)
cont.matrix <- makeContrasts(Erythroid.vs.Myeloid = Erythroid - Myeloid, levels = design)

##  Intensity binning
Intensity = rowMeans(data)
K = 30
library(limma)
q = quantile(Intensity,probs=seq(0,1,length.out=K+1))
I = cut(Intensity, breaks=q,labels=FALSE)
I[is.na(I)] = 1
pval = rep(NA_real_,nrow(data))
padj = rep(NA_real_,nrow(data))
for (k in 1:K) {
  fit = lmFit(data[I==k,], design)
  fit2 <- eBayes( contrasts.fit(fit, cont.matrix) )  
  pval[I == k] = fit2$p.value
  padj[I == k] = p.adjust(fit2$p.value,method="BH")
  print(sum(padj[I == k] <= 0.01,na.rm=TRUE))
}
names(padj)=RowName
hist(padj); length(padj)

## Intensity plot
# 3. erythroid vs myeloid
pdf('intensity_hsc_erythroid_vs_myeloid__vsn.pdf')
plot(rowMeans(data[,colnames(data) %in% c('Erythroid')]), rowMeans(data[,colnames(data) %in% c('Myeloid')]),pch=20,cex=0.7,xlab='log2(Erythroid)',ylab='log2(Myeloid)',cex.lab=1.5);abline(0,1,col='red',lwd=1)
graphics.off()

## Fold change
fold = rowMeans(data[,colnames(data)=='Erythroid']) - rowMeans(data[,colnames(data)=='Myeloid']); names(fold)=RowName

## Aggregate genes by highest variance across samples
variance = apply((data[,which(colnames(data) %in% c('Erythroid','Myeloid'))]),1,var); names(variance)=RowName
variance.aggregated.index =tapply(seq_along(variance), names(variance), function(x){
  m = max(variance[x])
  x[which(variance[x]==m)]
})
## Use this index for padj and fold
padj.aggregated = padj[variance.aggregated.index]
fold.aggregated = fold[variance.aggregated.index]
a = cbind(padj.aggregated, fold.aggregated)

## Export expression file
genes = c('Ebf1','Gata1','Gata2','Gata3','Cebpa','Ikzf1','Notch1','Pax5','Sfpi1','Tal1','Tcf3','Spi1')
T = data[variance.aggregated.index,];rownames(T) = RowName[variance.aggregated.index]
T[rownames(T) %in% genes,]
rownames(T)[which(rownames(T)=='Sfpi1')] = 'Spi1'
write.table(T, "/home/satoshi/new/perspective/hematopoietic_data/expression_hsc_GSE49991_Erythroid-Myeloid_vsn.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=TRUE)

## Export the padj and fold for later use
# 3. Erythroid.vs.Myeloid
rownames(a)[which(rownames(a)=='Sfpi1')] = 'Spi1'
write.table(a, "/home/satoshi/new/perspective/hematopoietic_data/padj_hscGSE49991_Erythroid-Myeloid.csv",sep="\t",row.names=TRUE,quote=FALSE, col.names=TRUE)

## For Isaac's tool
fc = fold.aggregated
#fc = fc[-grep("///",names(fc))]
names(fc)=toupper(names(fc))
fc = as.matrix(fc)
fc = ifelse(fc<0, 'DOWN', 'UP')
# Remove illegal names
fc = cbind(rownames(fc)[-grep( ("\\.\\d|.+?-.+?-.+?|^$|\\("), rownames(fc),perl=TRUE)] , fc[-grep( ("\\.\\d|.+?-.+?-.+?|^$|\\("), rownames(fc),perl=TRUE)] )
fc = fc[complete.cases(fc),]
fc[fc[,1]=='SFPI1',1]='SPI1'
write.table(fc, "/home/satoshi/new/perspective/hematopoietic_data/updown_SCC__GSE49991_Erythroid-Myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

########################################################
## Read in entire list of genes, padj, fold, metadata ##
########################################################
padj = read.csv("/home/satoshi/new/perspective/hematopoietic_data/padj_hscGSE49991_Erythroid-Myeloid.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
hist(padj[,'fold.aggregated'])
genes = c('Ebf1','Gata1','Gata2','Gata3','Cebpa','Ikzf1','Notch1','Pax5','Sfpi1','Tal1','Tcf3','Spi1','Gfi1','Gfi1b')
dim(padj[which(padj[,1]<=0.05 & abs(padj[,2])>=1),])
a = padj[which(rownames(padj) %in% genes),]
a[a[,1]<=0.05 & abs(a[,2])>=1,,drop=FALSE]
sig = padj[which(padj[,1]<=0.05  & abs(padj[,2])>=1),]
dim(sig)
dim(sig[sig[,'fold.aggregated']>=0,])
dim(sig[sig[,'fold.aggregated']<0,])
## Option 3: bioguo (http://www.bioguo.org/AnimalTFDB/download/gene_list_of_Mus_musculus.txt)
tfs = read.csv("/home/satoshi/new/perspective/bioguo_mouseTFs.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
dim(sig)
g1.tf = rownames(sig[which(rownames(sig) %in% tfs[,'mgi_symbol']),])
g1.tf = unique(g1.tf[!is.na(g1.tf)]); length(g1.tf)
g1.tf[which(g1.tf %in% keyTF)]

## Volcano plot
pdf('volcanoPlot_hscGSE49991_erythroid_vs_myeloid.pdf')
plot(padj[,'fold.aggregated'],-log10(padj[,'padj.aggregated']),col=ifelse(padj[,'padj.aggregated'] <= 0.05 & padj[,'fold.aggregated']>=1, "red",ifelse(padj[,'padj.aggregated'] <= 0.05 & padj[,'fold.aggregated']< -1, "blue", "black")),pch=20,main="Ectoderm/Mesoderm",xlab="log2(Ectoderm/Mesoderm)", ylab="-log10(adjusted p-value)",cex.axis=1.3,cex.lab=1.3,cex=1,xlim=c(-12,12),ylim=c(0,12))
graphics.off()

## Export significant genes for metacore
# 3.
write.table(g1.tf, "symbol_sig_hscGSE49991_erythroid_vs_myeloid_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)

############################################
## Seesaw preparation 1. Ratio difference ##
############################################
## Load the expression
# 1. vsn
expression = read.csv("/home/satoshi/new/perspective/hematopoietic_data/expression_hsc_GSE49991_Erythroid-Myeloid_vsn.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 2. quantile
#expression = read.csv("/home/satoshi/new/perspective/hematopoietic_data/expression_hsc_GSE49991_Erythroid-Myeloid_quantile.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

rownames(expression)[which(rownames(expression) == 'Sfpi1')] = 'Spi1'
RowName = rownames(expression)
colnames(expression) = gsub("\\.\\d+$","",colnames(expression),ignore.case=TRUE,perl=TRUE)

## 2. Take only TFs + DEGs
## bioguo (http://www.bioguo.org/AnimalTFDB/download/gene_list_of_Mus_musculus.txt)
tfs = read.csv("/home/satoshi/new/perspective/tf_symbols.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
I = which(toupper(rownames(expression)) %in% tfs[,1])
exp.tf = expression[I,]
exp.tf = cbind('Symbol'=rownames(exp.tf), exp.tf)
#exp.tf.variance = expression.variance[I,]

## 3. Compute expression distance between all pairs within a sample
## Ratios
ratio.outer = list()
for(i in 2:ncol(exp.tf)){
  m = outer(exp.tf[,i], exp.tf[,i], "-")
  ratio.outer[[i]] = m
}

## Take random combinations and compute ratio difference
# Option 1. FDCP - erythroid
n.replicates = 3
com = rbind(
  sample(which(colnames(exp.tf)=='FDCP'), n.replicates,replace=FALSE),
  sample(which(colnames(exp.tf)=='Erythroid'), n.replicates,replace=FALSE)
  )
# Option 2. FDCP - myeloid
n.replicates = 3
com = rbind(
  sample(which(colnames(exp.tf)=='FDCP'), n.replicates,replace=FALSE),
  sample(which(colnames(exp.tf)=='Myeloid'), n.replicates,replace=FALSE)
  )

M = matrix(nrow = (nrow(ratio.outer[[2]])^2 /2 - nrow(ratio.outer[[2]])/2), ncol = n.replicates)
for(i in 1:ncol(com)){
  comb = com[,i]  
  ## Compute the ratio difference
  ratio.diff = ratio.outer[[comb[1]]] - ratio.outer[[comb[2]]]
  # Option 1: Devide the ratio difference by the ESC ratio
  ratio.diff = ratio.diff / ratio.outer[[comb[1]]]
  # Option 2: Devide the ratio difference by the ESC ratio
  #ratio.diff.esc_ecto = ratio.diff.esc_ecto / abs(outer(exp.tf[,'ESC'], exp.tf[,'ESC'], "/"))  
  #rownames(ratio.diff) = colnames(ratio.diff) = rownames(ratio.outer[[comb[1]]]) = colnames(ratio.outer[[comb[1]]]) = rownames(ratio.outer[[comb[2]]]) = colnames(ratio.outer[[comb[2]]]) = exp.tf[,'Symbol']
  M[,i] = ratio.diff[upper.tri(ratio.diff)]
}
## Get rownames
I = which(upper.tri(ratio.outer[[2]]),arr.ind=TRUE)
genes = sprintf("%s__%s", exp.tf[,'Symbol'][I[,1]], exp.tf[,'Symbol'][I[,2]]); rownames(M) = genes
# Option 1.
colnames(M) = rep('FDCP_Erythroid',n.replicates)
# Option 2.
colnames(M) = rep('FDCP_Myeloid',n.replicates)

## Convert infinite values into real number
M[M==Inf] = 10000
M[M==-Inf] = -10000

## Intensity binning
intensity = cbind(
  exp.tf[match(exp.tf[I[,1],'Symbol'], exp.tf[,1]),'FDCP'],
  exp.tf[match(exp.tf[I[,2],'Symbol'], exp.tf[,1]),'FDCP'],
  #exp.tf[match(exp.tf[I[,1],'Symbol'], exp.tf[,1]),'Erythroid'],
  #exp.tf[match(exp.tf[I[,2],'Symbol'], exp.tf[,1]),'Erythroid']
  exp.tf[match(exp.tf[I[,1], 'Symbol'], exp.tf[,1]),'Myeloid'],
  exp.tf[match(exp.tf[I[,2], 'Symbol'], exp.tf[,1]),'Myeloid']
  )
intensity = rowSums(intensity,na.rm=TRUE)
names(intensity) = genes

library(geneplotter)
multidensity(M,xlim=c(-10,10))
## Optional: MAD normalization?
for(i in 1:ncol(M)){
  M[,i] = (M[,i] - median(M[,i],na.rm=TRUE)) / mad(M[,i],na.rm=TRUE)
}
multidensity(M,xlim=c(-10,10))

## Simple limma test
K = 30
library(limma)
q = quantile(intensity, probs=seq(0,1,length.out=K+1))
I = cut(intensity, breaks=q,labels=FALSE)
I[is.na(I)] = 1
pval = rep(NA_real_,nrow(M))
padj = rep(NA_real_,nrow(M))
for (k in 1:K) {
  ## median lift-up
#  s = apply(D[I == k,2:6],1,sd,na.rm=TRUE)
#  m = apply(D[I == k,2:6],1,mean,na.rm=TRUE)
#  n = apply(is.finite(as.matrix(D[I == k,2:6])),1,sum,na.rm=TRUE)
#  S = median(s, na.rm=TRUE)
#  snew = s
#  snew[s < S] = S
#  t <- sqrt(n) * m / snew
#  p.value <- 2*pt(-abs(t),df=n-1)
#  padj[I == k] = p.adjust(p.value,method="BH")
  
  ## limma
  fit = eBayes(lmFit(M[I == k,]))
  pval[I == k] = fit$p.value
  padj[I == k] = p.adjust(fit$p.value,method="BH")
  print(sum(padj[I == k] <= 0.05,na.rm=TRUE))
}
names(padj) = rownames(M)
hist(padj); length(padj)
J = which(padj <= 0.05)
length(J)

## Check important combinations
keyComb = c('Gata1__Spi1','Gfi1__Gfi1b')
padj[which(names(padj) %in% keyComb)]
#M[which(rownames(M) %in% keyComb),]

## Add fold change
ratio = cbind(padj,'mean.ratio.difference'=rowMeans(M))
ratio[rownames(ratio) %in% keyComb,]
#load(file='Myeloid_vsn_sigRatioDifference_combination.Rdata')
#load(file='Erythroid_vsn_sigRatioDifference_combination.Rdata')

# 1. Erythroid
write.table(ratio, "/home/satoshi/new/perspective/hematopoietic_data/RatioDifference_HSCandErythroid_HSCgse49991.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)
# 2. Myeloid
write.table(ratio, "/home/satoshi/new/perspective/hematopoietic_data/RatioDifference_HSCandMyeloid_HSCgse49991.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)

## Load the files
# 1. Erythroid
ratio = read.csv("/home/satoshi/new/perspective/hematopoietic_data/RatioDifference_HSCandErythroid_HSCgse49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# 2. Myeloid
ratio = read.csv("/home/satoshi/new/perspective/hematopoietic_data/RatioDifference_HSCandMyeloid_HSCgse49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")

# 1. Erythroid
sig = ratio[ratio[,'padj']<=0.05 & abs(ratio[,'mean.ratio.difference'])>=0.5, ]
# 2. Myeloid
sig = ratio[ratio[,'padj']<=0.05 & abs(ratio[,'mean.ratio.difference'])>=0.5, ]

keyComb = c('Gata1__Spi1')
sig[which(rownames(sig) %in% keyComb),]

## Separate genes
sig = sig[complete.cases(sig),]
g1.tf = unique(unlist(strsplit(rownames(sig),"__")))

## Export all the significant TFs
# 1. Erythroid
write.table(g1.tf, "/home/satoshi/new/perspective/hematopoietic_data/symbols_RatioDifference_HSCandErythroid_HSCgse49991.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
write.table(sig, "/home/satoshi/new/perspective/hematopoietic_data/sig_RatioDifference_HSCandErythroid_HSCgse49991.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)
# 2. Myeloid
write.table(g1.tf, "/home/satoshi/new/perspective/hematopoietic_data/symbols_RatioDifference_HSCandMyeloid_HSCgse49991.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
write.table(sig, "/home/satoshi/new/perspective/hematopoietic_data/sig_RatioDifference_HSCandMyeloid_HSCgse49991.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)

#########################################
## Seesaw preparation 2. Network files ##
#########################################
## Option 2: Take combinations whose genes are both present in g3
# 1. ESC-ectoderm
rd1 = read.csv("/home/satoshi/new/perspective/hematopoietic_data/sig_RatioDifference_HSCandErythroid_HSCgse49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote=""); g1 = rownames(rd1)
# 2. ESC-mesoderm
rd2 = read.csv("/home/satoshi/new/perspective/hematopoietic_data/sig_RatioDifference_HSCandMyeloid_HSCgse49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote=""); g2 = rownames(rd2)
# 3. ectoderm-mesoderm
g3 = read.csv("/home/satoshi/new/perspective/hematopoietic_data/symbol_sig_hscGSE49991_erythroid_vs_myeloid_4metacore.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote=""); g3[g3[,1]=='Sfpi1',1]='Spi1'

## Export all intersection ratio pairs
g12 = intersect(g1,g2)
G12 = g12[sign(rd1[match(g12, rownames(rd1)),2] * rd2[match(g12, rownames(rd2)),2]) == -1]
T = cbind(rd1[rownames(rd1) %in% G12,], rd2[rownames(rd2) %in% G12,])
"Gata1__Spi1" %in% rownames(T)
write.table(T, "/home/satoshi/new/perspective/hematopoietic_data/intersect_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",row.names=TRUE,quote=FALSE,col.names=TRUE)

# Intersection
g12 = intersect(g1,g2)
# Take combinations whose genes both exist in g3 and the ratio
#   difference is the opposite between the two
a = do.call('rbind',lapply(strsplit(g12,"__"),function(x){x}))
a = a[which(a[,1] %in% g3[,1] & a[,2]  %in% g3[,1]),]
a = sprintf("%s__%s", a[,1], a[,2])
G12 = a[sign(rd1[match(a, rownames(rd1)),2] * rd2[match(a, rownames(rd2)),2]) == -1]
g12.tf = unique(unlist(strsplit(G12,"__")))
g123 = intersect(g12.tf,g3[,1])
write.table(g123, "/home/satoshi/new/perspective/hematopoietic_data/symbols_DEbetweenErythroidMyeloid_RatioDifference_HSCgse49991_4metacore.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
write.table(G12, "/home/satoshi/new/perspective/hematopoietic_data/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Extract all interactions from transfac
#transfac = read.csv("/home/satoshi/new/ENCODE/transfac_experimental.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#a = transfac[which(transfac[,1] %in% toupper(g3[,1]) & transfac[,3] %in% toupper(g3[,1])),]
#write.table(a, "/home/satoshi/new/leiden/robert/transfac_symbol_sig_robert_d5_mpos_vs_d5_mneg_4metacore__vsn.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Make the phenotype file ##
# make the matrix
g3 = g3[,1]
M = matrix(nrow=length(g3), ncol=3)
rownames(M) = g3; colnames(M) = c('FDCP','Erythroid','Myeloid')
## 1. Parental cell is all 1    
M[,'FDCP'] = 1
## 2. Daughter cell types are differential expression
updown = read.csv("/home/satoshi/new/perspective/hematopoietic_data/updown_SCC__GSE49991_Erythroid-Myeloid.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
updown[updown[,1]=='SFPI1',1]='SPI1'
a = updown[match(toupper(rownames(M)), updown[,1]),]
a = cbind(a[,1], ifelse(a[,2]=="UP",1,0), ifelse(a[,2]=="UP",0,1))
M[,2] = a[,2]; M[,3] = a[,3]
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/Phenotype_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",row.names=TRUE,quote=FALSE,col.names=FALSE)

## Get network from metacore ##
# Option 1. transfac TF-DNA binding experimental
#transfac = read.csv("/home/satoshi/new/leiden/robert/transfac_symbol_sig_robert_d5_mpos_vs_d5_mneg_4metacore__vsn.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#colnames(transfac) = c('Gene.Symbol.From','Effect','Gene.Symbol.To')
# Option 2. metacore (all DETFs)
metacore.interaction = read.csv("/home/satoshi/new/perspective/hematopoietic_data/metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid_vsn_interaction.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
metacore.object = read.csv("/home/satoshi/new/perspective/hematopoietic_data/metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid_vsn_object.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 3. metacore (all DETFs with ratioDifference)

# Remove undirected interactions and extract needed information
a = metacore.interaction[which(
  (metacore.interaction$Effect=='Activation'
   | metacore.interaction$Effect=='Inhibition'
   | metacore.interaction$Effect=='Unspecified')
  & (metacore.interaction$Mechanism=='Transcription regulation'
     | metacore.interaction$Mechanism=='Binding')),
  c('Network.Object..FROM.','Effect','Network.Object..TO.','Mechanism')]

# Map objects to gene names
a = cbind(a,'Gene.Symbol.From'=toupper(metacore.object[match(a[,'Network.Object..FROM.'],metacore.object[,'Network.Object.Name']),'Gene.Symbol']))
a = cbind(a,'Gene.Symbol.To'=toupper(metacore.object[match(a[,'Network.Object..TO.'],metacore.object[,'Network.Object.Name']),'Gene.Symbol']))
a[,'Effect']=tolower(a[,'Effect'])

# Combine the two sources (remove redundancy)
literature = a[,c('Gene.Symbol.From','Effect','Gene.Symbol.To')]
literature = literature[complete.cases(literature),]
literature = cbind(as.character(literature[,1]), as.character(literature[,2]),as.character(literature[,3])); colnames(literature) = c('Gene.Symbol.From','Effect','Gene.Symbol.To')
literature = literature[literature[,1]!="" & literature[,3]!="",]
L = sprintf("%s__%s",literature[,1], literature[,3])
#T = sprintf("%s__%s",transfac[,1], transfac[,3])
#I = which(!T %in% L) # unique IDs
#b = rbind(literature, transfac[I,])

# Add self-loops
selfLoops = read.csv("/home/satoshi/common_files/metacore_selfLoops.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
a = unique(c(literature[,1], literature[,3]))
colnames(selfLoops) = colnames(literature)
literature = rbind(literature, selfLoops[selfLoops[,1] %in% a,])
ids = unique(c(as.character(literature[,1]), as.character(literature[,3])))

## Export a .sif file for cytoscape
write.table(literature, "/home/satoshi/new/perspective/hematopoietic_data/metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=TRUE)


## Get SCC ##


## Subset the phenotype file only for the genes present in the network
# 1. metacore + transfac
#net = read.csv("/home/satoshi/new/leiden/robert/transfac/SCC1_metacore_transfac_DEbetweenD5mposAndD5mneg_robert_padj0p05.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. metacore only
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/SCC1_metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

ids = unique(c(net[,1],net[,3]))
phe = read.csv("/home/satoshi/new/perspective/hematopoietic_data/Phenotype_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="",row.names=1)
rownames(phe) = toupper(rownames(phe))
p = phe[rownames(phe) %in% ids,]
write.table(p, "/home/satoshi/new/perspective/hematopoietic_data/BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",row.names=TRUE,quote=FALSE,col.names=FALSE)

net = net[net[,1] %in% rownames(p),]
net = net[net[,3] %in% rownames(p),]
# 1. metacore + transfac
#write.table(net, "/home/satoshi/new/leiden/robert/transfac/SCC1_metacore_transfac_DEbetweenD5mposAndD5mneg_robert_padj0p05.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
# 2. metacore
write.table(net, "/home/satoshi/new/perspective/hematopoietic_data/SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

## Make adjacency matrix
# 0.
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/SCC1_metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 1.
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/SCC1_1st_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2.
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/SCC1_adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3.
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree9_2000/SCC1_adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 4.
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_2/SCC1_adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv.sif",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")

enum = matrix(c(seq_along(unique(c(net[,1],net[,3]))), unique(c(net[,1],net[,3]))), ncol=2)
b = cbind(enum[match(net[,1],enum[,2]),1],enum[match(net[,3],enum[,2]),1]); class(b)='numeric'
length(unique(c(b[,1],b[,2]))) #unique node number 
b = cbind(b,net[,c(1,3)])
# 0.
write.table(b, "/home/satoshi/new/perspective/hematopoietic_data/enum_SCC1_metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE) 
# 1.
write.table(b, "/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_1000_1stNSCC/enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE) 
# 2.
write.table(b, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3.
write.table(b, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree9_2000/enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 4.
write.table(b, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_2/enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)


num = length(unique(c(as.numeric(b[,1]), as.numeric(b[,2]))))
mat = matrix(0,num,num)
rownames(mat)=colnames(mat)=enum[,2]
# 1. Indices of activation
act = net[net[,2]=='activation',]
mat[cbind(match(act[,1],rownames(mat)),match(act[,3],colnames(mat)))]=1
# 2. Indices of inhibition
inh = net[net[,2]=='inhibition',]
mat[cbind(match(inh[,1],rownames(mat)),match(inh[,3],colnames(mat)))]=-1
# 3. Indices of unspecified
uns = net[tolower(net[,2])=='unspecified',]
mat[cbind(match(uns[,1],rownames(mat)),match(uns[,3],colnames(mat)))]=2
## Export
# 0.
write.table(mat, "/home/satoshi/new/perspective/hematopoietic_data/adjacency_enum_SCC1_metacore_HSC_GSE49991_ratioDifference_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
# 1.
write.table(mat, "/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_1000_1stNSCC/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
write.table(mat, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
write.table(mat, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree9_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)
write.table(mat, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)

# Resize Booldata
booldata = read.csv("/home/satoshi/new/perspective/hematopoietic_data/BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
p = booldata[booldata[,1] %in% rownames(mat),]
write.table(p, "/home/satoshi/new/perspective/hematopoietic_data/BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)


#################################
## Optional: remove some nodes ##
#################################
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 1:
removeGenes = c('CREB3L2','JDP2','ATF5','MTA1','YBX1','TSC22D1')
# Option 2:
removeGenes = c('CREB3L2','JDP2','ATF5','MTA1','YBX1','TSC22D1','HHEX','SETDB1','KLF3','THRA','CREB3L4')
# Option 3:
removeGenes = c('CREB3L2','JDP2','ATF5','MTA1','YBX1','TSC22D1','HHEX','SETDB1','KLF3','THRA','CREB3L4','ESRRA','BCL11A','MYB','SMAD4','BCL6','NR3C1','ERG','TAL1','JUNB','SMAD1','RUNX3','FOSL2')

net = net[-which(net[,1] %in% removeGenes | net[,3] %in% removeGenes),]
enum = matrix(c(seq_along(unique(c(net[,1],net[,3]))), unique(c(net[,1],net[,3]))), ncol=2)
b = cbind(enum[match(net[,1],enum[,2]),1],enum[match(net[,3],enum[,2]),1]); class(b)='numeric'
length(unique(c(b[,1],b[,2]))) #unique node number 
b = cbind(b,net[,c(1,3)])
# 1.
write.table(b, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed/enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE) 
write.table(net, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)
write.table(net, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed3/SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

num = length(unique(c(as.numeric(b[,1]), as.numeric(b[,2]))))
mat = matrix(0,num,num)
rownames(mat)=colnames(mat)=enum[,2]
# 1. Indices of activation
act = net[net[,2]=='activation',]
mat[cbind(match(act[,1],rownames(mat)),match(act[,3],colnames(mat)))]=1
# 2. Indices of inhibition
inh = net[net[,2]=='inhibition',]
mat[cbind(match(inh[,1],rownames(mat)),match(inh[,3],colnames(mat)))]=-1
# 3. Indices of unspecified
uns = net[tolower(net[,2])=='unspecified',]
mat[cbind(match(uns[,1],rownames(mat)),match(uns[,3],colnames(mat)))]=2
# Export
write.table(mat, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)

# Resize Booldata
booldata = read.csv("/home/satoshi/new/perspective/hematopoietic_data/BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
p = booldata[booldata[,1] %in% rownames(mat),]
write.table(p, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed/BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",row.names=FALSE,quote=FALSE,col.names=FALSE)

# scp -P 8022 -r /home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed3/BoolData_HSCgse49991_Erythroid-Myeloid.txt  sokawa@access-gaia.uni.lu:matlab/malai/hsc_metacore/BoolData_HSCgse49991_Erythroid-Myeloid.txt
#  scp -P 8022 -r /home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed3/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv  sokawa@access-gaia.uni.lu:matlab/malai/hsc_metacore/
# oarsub -l nodes=2/core=12,walltime=120:00:00 "source /etc/profile; module load MATLAB; matlab -nodisplay -nosplash < runInitialStateContextualization_HSC.m"
#####################################





## Run Malai's tool ##


####################################################################
## Import the contextualized adjacency matrix and make a sif file ##
####################################################################
# 1.
#original.mat = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#pruned.mat = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/initialState_averaged_contextualized_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. some_nodes_removed_GATA2-selfActivation2/
#original.mat = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
#pruned.mat = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/initialState_averaged_contextualized_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
rowname = predicted.state[,1]
predicted.state = predicted.state[,-1]
a = cbind(rowMeans(predicted.state[,seq(1,ncol(predicted.state),2)]), rowMeans(predicted.state[,seq(2,ncol(predicted.state),2)]))
a[a>=0.5]=1
a[a<0.5]=0
rownames(a)=rowname
write.table(a, "/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/initialState_averaged_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)

B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
b = rowMeans(B)>=0.5
b[b==TRUE]=1
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
ind_optim = which(G!=0)  # nonzero elements
ind_actinh = which(G==2) # 2 unspecified
G1 = G
G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
G1[ind_optim[!b[1:length(ind_optim)]]] = 0
pruned.mat = G1


rownames(pruned.mat) = colnames(pruned.mat) = rownames(original.mat)
## Adjacency matrix to sif
# 1. acivation
I = which(pruned.mat==1,arr.ind=TRUE)
act = cbind(rownames(pruned.mat)[I[,1]], "activation",colnames(pruned.mat)[I[,2]])
# 2. inhibition
I = which(pruned.mat==-1,arr.ind=TRUE)
inh = cbind(rownames(pruned.mat)[I[,1]], "inhibition",colnames(pruned.mat)[I[,2]])

# 1.
#write.table(rbind(act,inh), "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/initialState_averaged_contextualized_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 2. some_nodes_removed_GATA2-selfActivation2/
#write.table(rbind(act,inh), "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/initialState_averaged_contextualized_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3. 
write.table(rbind(act,inh), "/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/initialState_averaged_contextualized_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.sif",sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)


### After Malai's tool ###


#############################################################
## Map back the significant, ratio-difference combinations ##
#############################################################
# Option 1: 
# Option 2: 
#predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/initialState_predicted_BoolData_HSC_GSE49991_RatioDifference_g3padj0p05.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 3: some_nodes_removed2
#predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 4: some_nodes_removed_GATA2-selfUnspeficied/
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# Option 5: some_nodes_removed_GATA2-selfActivation/
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 6: some_nodes_removed_GATA2-selfActivation2/
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 7: newBoolSim
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# Option 8: ordered_inhibitionDominant_GATA2activation_51
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 8. new67_ordered_GATA2activation_inhibitionDominant_200
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 9. new67_ordered_noGATA2_200
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_200/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 9. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 10. new67_ordered_GATA2unspecified_mBSim2_200
#predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_200/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 11. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 12. new51_ordered_GATA2unspecified_mBSim2_500
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 13. ordered_new67_GATA2activation_500_1stN
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2activation_500_1stN/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2activation_500_1stN/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2activation_500_1stN/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")

# 14. ordered_new67_GATA2unspecified_500_1stN
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 15. ordered_GATA2activation_1stNSCC_2000
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 16. ordered_GATA2inhibition_1stNSCC_2000
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 16. ordered_new67_GATA2unspecified_2000_1stN
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 17. ordered_GATA2unspecified_degree7_2000
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 18. ordered_GATA2unspecified_degree7_1500
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 19. ordered_GATA2activation_degree7_1500
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_degree7_1500/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_degree7_1500/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_degree7_1500/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 20. ordered_GATA2unspecified_degree7_1000
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 21. ordered_GATA2unspecified_degree7_1000_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 22. ordered_GATA2unspecified_degree7_1500_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 23. ordered_GATA2unspecified_degree7_2000_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 24. ordered_GATA2unspecified_degree7_1000_48
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_48/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_48/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_48/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 25. ordered_noGATA2_degree7_1000_41
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_noGATA2_degree7_1000_41/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_noGATA2_degree7_1000_41/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_noGATA2_degree7_1000_41/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 26. ordered_GATA2unspecified_degree7_1600_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 27. ordered_GATA2unspecified_degree7_1700_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
# 28. ordered_GATA2unspecified_degree7_1800_fixed
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")


# X. test
predicted.state = read.csv("/home/satoshi/new/perspective/hematopoietic_data/test/initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/test/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/test/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")


## Find seesaw pairs in the initial SCC ##
genes = rownames(G)
g = lapply(usedCombi[,1],function(x){
  a = strsplit(x,"__")
  if(sum(toupper(a[[1]]) %in% toupper(genes)) == 2){
    x
  }
})
names(g) = usedCombi[,1]
g = sapply(g, function(x){x[!sapply(x,is.null) ]})
g = g[!sapply(g, function(x){length(x)==0})]
length(g)
##

## Option1: For each combination, extract predicted states
#P = lapply(usedCombi[,1],function(x){
#  a = strsplit(x,"__")
#  b = predicted.state[match(toupper(a[[1]]), predicted.state[,1]),]
#  colnames(b) = c('Symbol','FDCP','Erythroid','Myeloid')
#  if(sum(is.na(b[,1]))==0){b}
#})
#names(P) = usedCombi[,1]
#P = P[!sapply(P, is.null)]
## Choose seesaw combinations
#p = lapply(P,function(x){
#  a = x[,-1]
#  if(sum(a[,1])==2){
#    if(sum(rowSums(a)==2)==2){
#      if(sum(a[,2])==1 && sum(a[,3])==1){
#        x
#     }
#    }
#  }
#})
#p = p[!sapply(p, is.null)]
# Save all the important files in 'result' directory

## Option 2: For multiple configurations
rownames(predicted.state) = predicted.state[,1]
predicted.state = predicted.state[,-1]
p = data.frame(t(predicted.state))
nConfiguration = ncol(predicted.state)/2
ind = gl(nConfiguration,2)
p = split(p, ind)
## For each combination, extract predicted states
P = lapply(usedCombi[,1],function(x){
  v = lapply(p,function(y){
    a = strsplit(x,"__")
    if(all(!is.na(match(toupper(a[[1]]),colnames(y))))){
      y[,match(toupper(a[[1]]),colnames(y))]
    }
  })
  v
})
names(P) = usedCombi[,1]
P = sapply(P, function(x){x[!sapply(x,is.null) ]})
P = P[!sapply(P, function(x){length(x)==0})]

# Option 1: 2 state opposite
p = lapply(P, function(y){
  v = lapply(y, function(x){
    a = t(x)
    if(sum(rowSums(a)==1)==2){
      if(sum(colSums(a)==1)==2){
        x
      }    
    }
  })
  v
})
# Option 2: 3 state seesaw
#p = lapply(P, function(y){
#  v = lapply(y, function(x){
#    a = t(x)
#    if(sum(a[,1])==2){
#      if(sum(rowSums(a)==2)==2){
#        if(sum(a[,2])==1 && sum(a[,3])==1){
#          x
#        }
#      }
#    }
#  })
#  v
#})
p = sapply(p, function(x){x[!sapply(x,is.null) ]})
p = p[!sapply(p, function(x){length(x)==0})]
# 1. NSC 1-usedCombi 1.
# 2.
# 3. some_nodes_removed2
#save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
# Option 4: some_nodes_removed_GATA2-selfUnspeficied/
#save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
# Option 5: some_nodes_removed_GATA2-selfActivation/
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
# Option 6: some_nodes_removed_GATA2-selfActivation2/
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 7: newBoolSim/
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 8: ordered_inhibitionDominant_GATA2activation_51
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 9: new67_ordered_GATA2activation_inhibitionDominant_200
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 10: new67_ordered_noGATA2_200
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 11: new67_ordered_noGATA2_mBSim1_200
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_mBSim1_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 12: new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 13: new67_ordered_GATA2unspecified_mBSim2_200
#save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 14: new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 15: new51_ordered_GATA2unspecified_mBSim2_500
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 16: ordered_new67_GATA2activation_500_1stN
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2activation_500_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 18: ordered_new67_GATA2unspecified_500_1stN
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 19. ordered_GATA2activation_1stNSCC_2000
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 20. ordered_GATA2inhibition_1stNSCC_2000
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 21. ordered_new67_GATA2unspecified_2000_1stN
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 22. ordered_GATA2unspecified_degree7_2000
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 23. ordered_GATA2unspecified_degree7_1500
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 24. ordered_GATA2unspecified_degree7_1000
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 25. ordered_GATA2unspecified_degree7_1000_fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 26. ordered_GATA2unspecified_degree7_1500_fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 27. ordered_GATA2unspecified_degree7_2000_fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 28. ordered_GATA2unspecified_degree7_1000_48
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_48/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 29. ordered_noGATA2_degree7_1000_41
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_noGATA2_degree7_1000_41/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 30. ordered_GATA2unspecified_degree7_1600_fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 31. ordered_GATA2unspecified_degree7_1700_fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 32. ordered_GATA2unspecified_degree7_1800_fixed
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')


# Option X. test
save(P,p, nConfiguration, file='/home/satoshi/new/perspective/hematopoietic_data/test/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')

## Compute the probability of each combination
# 1.
#load(file='usedCombi_initialState_predicted_BoolData_NSC_GSE9566_Neuron-Astroglia_RatioDifference_g3padj0p01.Rdata')
# 2.
# 3. some_nodes_removed2
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
# Option 4: some_nodes_removed_GATA2-selfUnspeficied/
#load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 5: some_nodes_removed_GATA2-selfActivation/
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 6: some_nodes_removed_GATA2-selfActivation2/
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 7: newBoolSim/
load(file='/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 8: ordered_inhibitionDominant_GATA2activation_51
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 9: new67_ordered_GATA2activation_inhibitionDominant_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 10: new67_ordered_noGATA2_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 11: new67_ordered_noGATA2_mBSim1_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_noGATA2_mBSim1_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 12: new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 13: new67_ordered_GATA2unspecified_mBSim2_200
#load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 14: new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 15: new51_ordered_GATA2unspecified_mBSim2_500
load(file='/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 16: ordered_new67_GATA2activation_500_1stN
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2activation_500_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 18: ordered_new67_GATA2unspecified_500_1stN
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 19: ordered_GATA2activation_1stNSCC_2000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 20: ordered_GATA2inhibition_1stNSCC_2000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 21: ordered_new67_GATA2unspecified_2000_1stN
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 22: ordered_GATA2unspecified_degree7_2000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 23: ordered_GATA2unspecified_degree7_1500
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 24: ordered_GATA2unspecified_degree7_1000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 25: ordered_GATA2unspecified_degree7_1000_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 26: ordered_GATA2unspecified_degree7_1500_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 27: ordered_GATA2unspecified_degree7_2000_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 28: ordered_GATA2unspecified_degree7_1000_48
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_48/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 29: ordered_noGATA2_degree7_1000_41
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_noGATA2_degree7_1000_41/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 30: ordered_GATA2unspecified_degree7_1600_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 31: ordered_GATA2unspecified_degree7_1700_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')
# Option 32: ordered_GATA2unspecified_degree7_1800_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')

# Option X: test
load(file='/home/satoshi/new/perspective/hematopoietic_data/test/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata')


fraction = lapply(p, function(y){
  length(y)/nConfiguration*100
})
# Match with ratio-stats seesaw
names(fraction)[which(names(fraction) %in% usedCombi[,1])]
 c('Gata1__Spi1','Fos__Gata1','Gata1__Gata2') %in% names(fraction)[which(names(fraction) %in% usedCombi[,1])] 



####################################################################
## Import the contextualized adjacency matrix and make a sif file ##
####################################################################
# 1.


####################################################
## Compute gene retroactivity of each combination ##
####################################################




#################################################################
## Load all best configurations ##
##################################
# 1. some_nodes_removed2  (0.05, 0.5)
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 2. some_nodes_removed_GATA2-selfUnspeficied
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. some_nodes_removed_GATA2-selfActivation
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 4. some_nodes_removed_GATA2-selfActivation2
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 5. newBoolSim/
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 6. ordered_inhibitionDominant_GATA2activation_51
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 7. new67_ordered_GATA2activation_inhibitionDominant_200
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 8. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 9. new51_ordered_GATA2unspecified_mBSim2_500
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 10. ordered_new67_GATA2unspecified_500_1stN
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 11. ordered_GATA2activation_1stNSCC_2000
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 12. ordered_GATA2inhibition_1stNSCC_2000
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 13. ordered_new67_GATA2unspecified_2000_1stN
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 12. ordered_GATA2unspecified_degree7_2000
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 13. ordered_GATA2unspecified_degree7_1500
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 14. ordered_GATA2unspecified_degree7_1000
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 15. ordered_GATA2unspecified_degree7_1000_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 16. ordered_GATA2unspecified_degree7_1500_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 17. ordered_GATA2unspecified_degree7_2000_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 18. ordered_GATA2unspecified_degree7_1600_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 19. ordered_GATA2unspecified_degree7_1700_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 20. ordered_GATA2unspecified_degree7_1800_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")


# X. test
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/test/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/test/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")


ind_optim = which(G!=0)  # nonzero elements
ind_actinh = which(G==2) # 2 unspecified
scc = list()
for (i in 1:ncol(B)){
  b = B[,i]
  G1 = G
  G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
  G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
  G1[ind_optim[!b[1:length(ind_optim)]]] = 0

  # 1. acivation
  I = which(G1==1,arr.ind=TRUE)
  act = cbind(rownames(G1)[I[,1]], "->",colnames(G1)[I[,2]])
  # 2. inhibition
  I = which(G1==-1,arr.ind=TRUE)
  inh = cbind(rownames(G1)[I[,1]], "-|",colnames(G1)[I[,2]])

  write.table(rbind(act,inh), "/home/satoshi/new/perspective/hematopoietic_data/SCC_temp/scc.sif",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
  system('perl /home/satoshi/programmes/perl/isaac_SCC.pl -f /home/satoshi/new/perspective/hematopoietic_data/SCC_temp/scc.sif')
  g = read.csv("/home/satoshi/new/perspective/hematopoietic_data/SCC_temp/scc.sif.scc",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
  name = paste('conf',i,sep="")
  g = g[-1,]
  scc[[i]] = g
}
# 1. some_nodes_removed2
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/all_scc.Rdata')
# 2. some_nodes_removed_GATA2-selfUnspeficied
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/all_scc.Rdata')
# 3. some_nodes_removed_GATA2-selfActivation
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/all_scc.Rdata')
# 4. some_nodes_removed_GATA2-selfActivation2
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/all_scc.Rdata')
# 5. newBoolSim
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/all_scc.Rdata')
# 6. ordered_inhibitionDominant_GATA2activation_51
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/all_scc.Rdata')
# 7. new67_ordered_GATA2activation_inhibitionDominant_200
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/all_scc.Rdata')
# 8. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/all_scc.Rdata')
# 9. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/all_scc.Rdata')
# 10. new51_ordered_GATA2unspecified_mBSim2_500
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/all_scc.Rdata')
# 11. ordered_new67_GATA2unspecified_500_1stN
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/all_scc.Rdata')
# 12. ordered_GATA2activation_1stNSCC_2000
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/all_scc.Rdata')
# 13. ordered_GATA2inhibition_1stNSCC_2000
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/all_scc.Rdata')
# 14. ordered_new67_GATA2unspecified_2000_1stN
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/all_scc.Rdata')
# 15. ordered_GATA2unspecified_degree7_2000
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/all_scc.Rdata')
# 16. ordered_GATA2unspecified_degree7_1500
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/all_scc.Rdata')
# 17. ordered_GATA2unspecified_degree7_1000
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/all_scc.Rdata')
# 18. ordered_GATA2unspecified_degree7_1000_fixed
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/all_scc.Rdata')
# 19. ordered_GATA2unspecified_degree7_1500_fixed
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/all_scc.Rdata')
# 20. ordered_GATA2unspecified_degree7_2000_fixed
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/all_scc.Rdata')
# 21. ordered_GATA2unspecified_degree7_1600_fixed
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/all_scc.Rdata')
# 22. ordered_GATA2unspecified_degree7_1700_fixed
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/all_scc.Rdata')
# 23. ordered_GATA2unspecified_degree7_1800_fixed
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/all_scc.Rdata')

# X. test
save(scc, file='/home/satoshi/new/perspective/hematopoietic_data/test/all_scc.Rdata')


## For each SCC, check the occurrence of seesaw pairs in each SCC
# 1. some_nodes_removed2
#load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
#usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
#load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/all_scc.Rdata')
# 2.  some_nodes_removed_GATA2-selfUnspeficied
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfUnspeficied/all_scc.Rdata')
# 3.  some_nodes_removed_GATA2-selfActivation
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/all_scc.Rdata')
# 4.  some_nodes_removed_GATA2-selfActivation2
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/all_scc.Rdata')
# 5. newBoolSim 
load(file='/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/newBoolSim/all_scc.Rdata')
# 6. ordered_inhibitionDominant_GATA2activation_51
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_inhibitionDominant_GATA2activation_51/all_scc.Rdata')
# 7. new67_ordered_GATA2activation_inhibitionDominant_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2activation_inhibitionDominant_200/all_scc.Rdata')
# 8. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/all_scc.Rdata')
# 9. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/all_scc.Rdata')
# 10. new51_ordered_GATA2unspecified_mBSim2_500
load(file='/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/all_scc.Rdata')
# 11. ordered_new67_GATA2unspecified_500_1stN
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/all_scc.Rdata')
# 12. ordered_GATA2activation_1stNSCC_2000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/all_scc.Rdata')
# 13. ordered_GATA2inhibition_1stNSCC_2000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2inhibition_1stNSCC_2000/all_scc.Rdata')
# 14. ordered_new67_GATA2unspecified_2000_1stN
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/all_scc.Rdata')
# 15. ordered_GATA2unspecified_degree7_2000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/all_scc.Rdata')
# 16. ordered_GATA2unspecified_degree7_1500
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/all_scc.Rdata')
# 17. ordered_GATA2unspecified_degree7_1000
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000/all_scc.Rdata')
# 18. ordered_GATA2unspecified_degree7_1000_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/all_scc.Rdata')
# 19. ordered_GATA2unspecified_degree7_1500_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/all_scc.Rdata')
# 20. ordered_GATA2unspecified_degree7_2000_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/all_scc.Rdata')
# 21. ordered_GATA2unspecified_degree7_1600_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/all_scc.Rdata')
# 22. ordered_GATA2unspecified_degree7_1700_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/all_scc.Rdata')
# 23. ordered_GATA2unspecified_degree7_1800_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/all_scc.Rdata')

# X. test
load(file='/home/satoshi/new/perspective/hematopoietic_data/test/usedCombi_initialState_predicted_BoolData_HSCgse49991_Erythroid-Myeloid.Rdata') 
usedCombi = read.csv("/home/satoshi/new/perspective/hematopoietic_data/test/used_combinations_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
load(file='/home/satoshi/new/perspective/hematopoietic_data/test/all_scc.Rdata')

fraction = lapply(p, function(y){
  length(y)/nConfiguration*100
})
# Match with ratio-stats seesaw
seesaw = toupper(names(fraction)[which(names(fraction) %in% usedCombi[,1])])

# Check if both genes are present in each SCC
scc.fraction = lapply(seesaw, function(x){
  s = strsplit(x,"__")
  count = 0
  count.direct.connection = 0
  for(i in 1:length(scc)){
    genes = unique(c(scc[[i]][,1], scc[[i]][,3]))
    if(sum(s[[1]] %in% genes)==2){
      count = count+1
    }
    if(sum(scc[[i]][,1] %in% s[[1]][1] & scc[[i]][,3] %in% s[[1]][2]) > 0
       | sum(scc[[i]][,3] %in% s[[1]][1] & scc[[i]][,1] %in% s[[1]][2]) > 0
       ){
      count.direct.connection = count.direct.connection + 1
    }
  }    
  c(count / length(scc)*100, count.direct.connection / length(scc)*100)
})
names(scc.fraction) = seesaw

## Combine seesaw fraction and SCC fraction
scc.pairs = cbind(do.call('rbind',lapply(fraction,function(x){x})), do.call('rbind',lapply(scc.fraction,function(x){x})))
colnames(scc.pairs) = c('seesaw.state.fraction','scc.fraction','direct.connection')
scc.pairs[rev(order(scc.pairs[,2])),]
scc.pairs[rev(order(scc.pairs[,3])),]


## Check the ratioDifference
ratio = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/intersect_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
a = ratio[rownames(ratio) %in% rownames(scc.pairs) ,]
scc.pairs = cbind(scc.pairs, ratio[match( rownames(scc.pairs), rownames(ratio)),])
scc.pairs[rev(order(scc.pairs[,2])),]
save(scc.pairs, file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/scc_pairs.Rdata')

## Reload
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_attractor_200/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/new51_ordered_GATA2unspecified_mBSim2_500/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_500_1stN/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_1stNSCC_2000/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_new67_GATA2unspecified_2000_1stN/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_degree7_1500/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1000_fixed/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_fixed/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1700_fixed/scc_pairs.Rdata')
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/scc_pairs.Rdata')


# Increase stringency
scc.pairs.sig = scc.pairs[
  scc.pairs[,'padj']<=0.05 & abs(scc.pairs[,'mean.ratio.difference'])>=1 &
  scc.pairs[,'padj.1']<=0.05 & abs(scc.pairs[,'mean.ratio.difference.1'])>=1 &
  scc.pairs[,'seesaw.state.fraction']>=90 &
  scc.pairs[,'scc.fraction']>=90
  ,]
#scc.pairs.sig[rev(order(scc.pairs.sig[,1])),]
scc.pairs.sig[rev(order(scc.pairs.sig[,3])),]

## 


## Export the arrow-verion of the sif file
T = read.csv("GSE9566_ratioDifference_padj0p05_metacoreOnly/initialState_averaged_contextualized_adjacency_SCC1_GSE9566_ratioDifference_padj0p05_metacoreOnly.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
T[,2] = ifelse(T[,2]=='activation','->', '-|')
T = T[!duplicated(T),]
#write.table(T, "/home/satoshi/new/perspective/neural_data/arrow.csv",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

###################################################################
## Convert SCCs into adjacency matrix for perturbation in matlab ##
###################################################################
# 1.
load(file='/all_scc.Rdata')
# 2. some_nodes_removed_GATA2-selfActivation
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/all_scc.Rdata')
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 3. some_nodes_removed_GATA2-selfActivation2
load(file='/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/all_scc.Rdata')
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 4. new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 5. ordered_GATA2unspecified_degree7_2000_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
# 6. ordered_GATA2unspecified_degree7_1600_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")


ind_optim = which(G!=0)  # nonzero elements
ind_actinh = which(G==2) # 2 unspecified

# 1. convert each configuration into an adjacency matrix
# 2. put 0 to non-SCC part in the adjacency matrix 
# 3. convert the adjacency matrix into the vector representation

M = matrix(nrow=length(G[G!=0]), ncol=ncol(B))
I.G = which(G!=0,arr.ind=TRUE)
rownames(M) = sprintf("%d__%d",I.G[,1],I.G[,2])
for (i in 1:ncol(B)){
#M = apply(B,2,function(x){
  ## Convert each configuration into an adjacency matrix
  b = B[,i]
  I.G = which(G!=0,arr.ind=TRUE)
  I.G = cbind(I.G,'interact'=0)

  ## Contextualization
  G1 = G  
  G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
  G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
  G1[ind_optim[!b[1:length(ind_optim)]]] = 0
  I.G[,3] = G1[I.G[,1:2]]
  
  ## Put 0 to non-SCC part in the adjacency matrix 
  s = scc[[i]]

  ## Find array indices of the SCC and maintain only those array indices and put 0 to all the others
  I = cbind(match(s[,1], rownames(G1)), match(s[,3], colnames(G1)))
  a1 = sprintf("%d__%d",I.G[,1],I.G[,2])
  a2 = sprintf("%d__%d",I[,1],I[,2])
  rownames(I.G) = a1
  I.G[which(!a1 %in% a2),3] = 0 # edges not present in the SCC
  I.G[,3]
  M[,i] = I.G[match(rownames(M), rownames(I.G)),3]
}

## Export for the matlab "doPerturbation.m"
# 1.

# 2. some_nodes_removed_GATA2-selfActivation
write.table( which(G!=0), "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 3. some_nodes_removed_GATA2-selfActivation2
write.table( which(G!=0), "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 4. 
write.table( which(G!=0), "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation2/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 5. 
write.table( which(G!=0), "/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
# 7. 1600_fixed
write.table( which(G!=0), "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/pertubation_nonzeroIndices.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)
write.table(M, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/pertubation_adjacencyVectors.txt",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)


    ## Start running Matlab for finding DEPCs ##


########################################################
## Check degrees of each node in the original network ##
########################################################
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
degree = do.call('rbind',lapply(rownames(G),function(x){
  sum(G[rownames(G) %in% x,]!=0) + sum(G[,colnames(G) %in% x]!=0)
}))
rownames(degree) = rownames(G)
degree[rev(order(degree)),]


#####################
## Check intensity ##
#####################
library(limma)
library(geneplotter)
data = read.csv("/home/satoshi/new/perspective/hematopoietic_data/data_GSE49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="",row.names=1)
data = data[complete.cases(data),]
sample_table = read.csv("/home/satoshi/new/perspective/hematopoietic_data/sample_table_hematopoietic.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
colnames(data) = sample_table[match(colnames(data),sample_table[,1]),2]
RowName = data[,1]
# 1. Only end points
data = data[,colnames(data) %in% c('FDCP','Erythroid_168h','Myeloid_168h')]
colnames(data) = gsub("\\.\\d+$","",colnames(data),ignore.case=TRUE,perl=TRUE)
colnames(data) = gsub("_.+$","",colnames(data),ignore.case=TRUE,perl=TRUE)

library(vsn)
v = vsn2(as.matrix(2**data))
data = exprs(v)
multidensity(data)
# Option 2: quantile
#data = normalizeQuantiles(data)
#multidensity(data)

## Fold change
fold = rowMeans(data[,colnames(data)=='Erythroid']) - rowMeans(data[,colnames(data)=='Myeloid']); names(fold)=RowName

## Aggregate genes by highest variance across samples
variance = apply((data[,which(colnames(data) %in% c('Erythroid','Myeloid'))]),1,var); names(variance)=RowName
variance.aggregated.index =tapply(seq_along(variance), names(variance), function(x){
  m = max(variance[x])
  x[which(variance[x]==m)]
})
data.aggregated = data[variance.aggregated.index,]
RowName.aggregated = RowName[variance.aggregated.index]

m = match(c('Gata1','Sfpi1','Gata2','Cebpa','Rxra'), RowName.aggregated)
data.aggregated[m,]


##############################################
## Check the frequency of positive circuits ##
##############################################

## Run 'findAllCircuits.m' in Matlab ##

## 2. gata2-selfActivation
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
I.edges = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/Pcircuits_in_all_SCCs_HSC_edgeIndices.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
I.edges = as.matrix(I.edges)
I.nodes = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/Pcircuits_in_all_SCCs_HSC_nodeIndices.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
I.nodes = as.matrix(I.nodes)
mode.of.action = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/Pcircuits_in_all_SCCs_HSC_modeOfAction.csv",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
mode.of.action = as.matrix(mode.of.action)
mode.of.action[!is.finite(mode.of.action)]=0

## Convert node indices into names
genes = rownames(G)
I.nodes[I.nodes==0]=NA
I.genes = apply(I.nodes,2,function(x){
  genes[x]
})
I.genes[is.na(I.genes)]=0

## Make a unique PC identifier
M = cbind(I.genes, I.edges, mode.of.action)
ID = apply(M,1,function(x){
  paste(x,collapse="_")
})
ID.freq = table(ID)
ID.freq[order(ID.freq)]
length(ID)
length(ID.freq)

## Find the most frequent circuits with candidate seesaw pairs
scc.pairs.sig[rev(order(scc.pairs.sig[,3])),]
keyP = c('GATA1', 'SPI1')
ID.freq = as.data.frame(ID.freq)
cir = do.call("rbind",apply(ID.freq,1,function(x){
  a = strsplit(x[1],"_")
  if(all(keyP %in% a[[1]])){
    x
  }
}))
cir[order(as.numeric(cir[,2])),]

## Most frequent circuit of all
ID.freq[ID.freq[,2]==max(ID.freq[,2]),]

## Extract the network of defined genes from each SCC
# Indices of non-0 elements
I_G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/pertubation_nonzeroIndices.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
# SCC vector formats
V = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/pertubation_adjacencyVectors.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")
keyG = c('GATA1', 'SPI1', 'GATA2')
keyPCs = list()
for (i in 1:ncol(V)){
  ## Make adjacency matrix
  b = V[,i]
  G1 = G
  G1[I_G[,1]]=b

  ## extract the genes of interest
  pc = G1[match(keyG,rownames(G1)), match(keyG,colnames(G1))]  
  if( (sum(pc==-1) %% 2) ==0 ){
    keyPCs[[i]] = pc
  }
}
## Remove the NULL elements and 
keyPCs = keyPCs[!sapply(keyPCs, is.null)]
v.keyPCs = do.call('rbind',lapply(keyPCs,function(x){
  a = as.vector(x)
  a = paste(a,collapse="_")
}))
b = table(v.keyPCs)
f.keyPCs = lapply(names(b),function(x){
  m = matrix(as.numeric(strsplit(x,"_")[[1]]),ncol=length(keyG))
  rownames(m) = colnames(m) = keyG
  m
})

## Convert each unique PC into a sif file
for (i in 1:length(f.keyPCs)){
  mat = f.keyPCs[[i]]  
  if(!is.null(mat)){    
    # 1. acivation
    I = which(mat==1,arr.ind=TRUE)
    act = cbind(rownames(mat)[I[,1]], "activation",colnames(mat)[I[,2]])
    # 2. inhibition
    I = which(mat==-1,arr.ind=TRUE)
    inh = cbind(rownames(mat)[I[,1]], "inhibition",colnames(mat)[I[,2]])
    out = paste("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/uniqueKeyPC_",i,"_",b[i],".sif", sep="")
    write.table(rbind(act,inh), out ,sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
  }
}


#############################################
## Export sif files with different cutoffs ##
#############################################
# 1.
original.mat = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
pruned.mat = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed2/initialState_averaged_contextualized_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt",sep=" ",header=FALSE,stringsAsFactors=FALSE,quote="")

# 2. some_nodes_removed_GATA2-selfActivation/
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv('/home/satoshi/new/perspective/hematopoietic_data/some_nodes_removed_GATA2-selfActivation/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt',sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="");
# 3. ordered_GATA2unspecified_degree7_1500
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv('/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt',sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="");
# 4. ordered_GATA2unspecified_degree7_1600_fixed
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
G = as.matrix(G)
B = read.csv('/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/initialState_predicted_contextualized_best_adjacency_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.txt',sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="");


cutoff = c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 1)
for (i in 1:length(cutoff)){
  # Decide the cutoff  
  x = cutoff[i]

  # 1. mean
  b = rowMeans(B)
  # 2. single best one
  #b = B[,100]
  
  b[b <= x]=0 
  ind_data = nrow(G)
  ind_optim = which(G!=0)  # nonzero elements
  ind_actinh = which(G==2); # 2 unspecified 
  G1 = G
  G1[ind_actinh[!!b[(length(ind_optim)+1):length(b)]]] = 1
  G1[ind_actinh[!b[(length(ind_optim)+1):length(b)]]] = -1
  G1[ind_optim[!b[1:length(ind_optim)]]] = 0
  ## Export adjacency
  out = paste("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/adjacency_bestConfiguration_",x,".txt", sep="")
  write.table(G1, out ,sep="\t",row.names=TRUE,quote=FALSE, col.names=FALSE)
  
  ## Adjacency matrix to sif
  # 1. acivation
  I = which(G1==1,arr.ind=TRUE)
  act = cbind(rownames(G1)[I[,1]], "activation",colnames(G1)[I[,2]])
  # 2. inhibition
  I = which(G1==-1,arr.ind=TRUE)
  inh = cbind(rownames(G1)[I[,1]], "inhibition",colnames(G1)[I[,2]])
  out = paste("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/bestConfiguration_",x,".sif", sep="")
  write.table(rbind(act,inh), out ,sep="\t",row.names=FALSE,quote=FALSE, col.names=FALSE)
}

#########################################
## Check the number of out-going edges ##
#########################################
# 1. ordered_GATA2unspecified_degree7_1600_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/scc_pairs.Rdata')
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/bestConfiguration_0.5.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

# Increase stringency
scc.pairs.sig = scc.pairs[
  scc.pairs[,'padj']<=0.05 & abs(scc.pairs[,'mean.ratio.difference'])>=1 &
  scc.pairs[,'padj.1']<=0.05 & abs(scc.pairs[,'mean.ratio.difference.1'])>=1 &
  scc.pairs[,'seesaw.state.fraction']>=90 &
  scc.pairs[,'scc.fraction']>=90 
  &  scc.pairs[,'direct.connection']>=50
  ,]
scc.pairs.sig[rev(order(scc.pairs.sig[,1])),]
scc.pairs.sig[rev(order(scc.pairs.sig[,3])),]
g = rownames(scc.pairs.sig[rev(order(scc.pairs.sig[,3])),])

d = lapply(g,function(x){
  s = strsplit(x,"__")
  min(c(sum(net[,1] %in% toupper(s[[1]][2])), sum(net[,1] %in% toupper(s[[1]][2]))))
})
names(d) = g


################################################################
## Check the frequency of DEPCs after 'runFindAllDEPCs_HSC.m' ##
################################################################
## load all SCCs
# 1. ordered_GATA2unspecified_degree7_2000_fixed
#load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_2000_fixed/all_scc.Rdata')
#scc = lapply(scc,function(x){
#  x[x[,2]=='->',2] = 'activation'
#  x[x[,2]=='-|',2] = 'inhibition'
#  x
#})
#s.uniq = scc[!duplicated(scc)]
# 2. ordered_GATA2unspecified_degree7_1600_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/all_scc.Rdata')
scc = lapply(scc,function(x){
  x[x[,2]=='->',2] = 'activation'
  x[x[,2]=='-|',2] = 'inhibition'
  x
})
s.uniq = scc[!duplicated(scc)]


## Load DEPC file
depcs = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/DEPCs/vector_adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
l = apply(depcs,2,function(x){ paste(x,sep='',collapse='')})
length(table(l))

# Subset for specific genes
p = "GATA1__SPI1"
I1 = c(which(depcs[rownames(depcs) %in% p,]!=0))
p = "IRF1__CUX1"
I2 = which(depcs[rownames(depcs) %in% p,]!=0)
p = "GATA1__GATA2"
I3 = which(depcs[rownames(depcs) %in% p,]!=0)
p = "GATA1__CEBPA"
I4 = which(depcs[rownames(depcs) %in% p,]!=0)
p = "GATA1__FOS"
I5 = which(depcs[rownames(depcs) %in% p,]!=0)

# Option 1: more than one pair
I = NULL
I = intersect(I1, I3)
I = intersect(I1, I4)
I = intersect(I1, I5)
I = intersect(I1, I2)
I = intersect(intersect(I1, I3),I4)
I = intersect(intersect(I1, I4),I5)
I = intersect(intersect(intersect(I1, I2), I4),I5)
I = intersect(intersect(intersect(I1, I3), I4),I5)
sub = depcs[,I]

# Option 2: only one pair
sub = depcs[,which(depcs[rownames(depcs)==p,]!=0)]


l = apply(sub,2,function(x){ paste(x,sep='',collapse='')})
b = split(l,l)  # binary -- file names
rep.file = do.call('rbind',lapply(b,function(x){names(x[1])}))
## Find the minimum DEPC
freq <- do.call('rbind',lapply(l, function(z){ sapply(gregexpr("0",z),function(y)if(y[[1]]!=-1) length(y) else 0)}))
freq = freq[rev(order(freq[,1])),] #descending

# match freq and rep.file
A = cbind(rep.file,'size'=freq[match(rep.file, names(freq))])
A = A[rev(order(A[,2])),]
b = A[1:100,1]
#b = A[,1]
names(b)=NULL

# for each motif, compute the frequency
F = lapply(b,function(y){
  #a = names(x[1])
  # read in the file
  infile = outfile = paste("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/DEPCs/", y,".sif",sep="")
  motif = read.csv(infile,sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
  v = lapply(s.uniq,function(x){
    g = unique(c(motif[,1], motif[,3]))
    sub = x[x[,1] %in% g & x[,3] %in% g,]
    a = sprintf("%s__%s__%s",sub[,1],sub[,2],sub[,3])
    m = sprintf("%s__%s__%s",motif[,1],motif[,2],motif[,3])
    all(m %in% a) & all(a %in% m)
  })
  per = sum(v==TRUE)/length(v)

  n.s = length(unique(c(motif[,1],motif[,3])))
  e.s = nrow(motif)
  genes = paste(unique(c(motif[,1],motif[,3])),collapse=";")
  # return the number of nodes and interactions, and frequency
  c(n.s, e.s, per*100, genes)  
})
names(F) = b

p = "GATA1__SPI1__CEBPA"
p = "GATA1__SPI1__FOS"
p = "GATA1__SPI1__IRF1__CUX1"
p = "GATA1__SPI1__GATA2"
p = "GATA1__SPI1__CEBPA__FOS"
p = "GATA1__SPI1__CEBPA__FOS__IRF1__CUX1"
p = "GATA1__SPI1__GATA2__CEBPA__FOS"
save(F, file=paste("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_",p,".Rdata",sep=""))

## Reload
# 1. GATA1__SPI1
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1.Rdata')
# 2. GATA1__GATA2
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__GATA2.Rdata')
# 3. IRF1__CUX1
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_IRF1__CUX1.Rdata')
# 4. GATA1__CEBPA
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__CEBPA.Rdata')
# 4. GATA1__FOS
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__FOS.Rdata')
# 6. GATA1__SPI1__CEBPA
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__CEBPA.Rdata')
# 6. GATA1__SPI1__FOS
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__FOS.Rdata')
# 6. GATA1__SPI1__FOS__CEBPA
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__CEBPA__FOS.Rdata')
# 6. GATA1__SPI1__IRF1__CUX1
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__IRF1__CUX1.Rdata')
# 6. GATA1__SPI1__FOS__CEBPA__IRF1__CUX1
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__CEBPA__FOS__IRF1__CUX1.Rdata')
# 6. GATA1__SPI1__GATA2
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__GATA2.Rdata')
# 7. GATA1__SPI1__GATA2__CEBPA__FOS
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/minimumDEPCs_GATA1__SPI1__GATA2__CEBPA__FOS.Rdata')


T = do.call('rbind',lapply(F,function(x){x}))

head(T[rev(order(as.numeric(T[,3]))),])
#head(T[order(as.numeric(T[,1])),])
#T[order(as.numeric(T[,1])),]

## Find motifs with each node at least with one activation input  
# For each motif, check if all rows have at least one "1"
#for (i in 1:nrow(T)){
v = do.call('rbind',lapply(rownames(T),function(x){
  infile = paste("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/DEPCs/", x,sep="")
  #print(infile)
  motif = read.csv(infile,sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
  motif = motif[,-ncol(motif)]
  if( sum(colSums(motif==1)==0) ==0 ){
    'Yes'
  } else{
    'No'
  }
}))
rownames(v) = rownames(T)
T = cbind(T,v)
head(T[rev(order(as.numeric(T[,3]))),],10)

########################################################
##  Find motifs in all the SCCs and compute fractions ##
########################################################
# Bitoggle switch
motif = read.csv("/home/satoshi/new/perspective/hematopoietic_data/new67_ordered_GATA2unspecified_mBSim2_HSCfixed_200/sub_DEPCs/adjacency_DEPC_298627.adj.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
motif = matrix(c(
  'GATA1','SPI1','SPI1',
  'activation','activation','inhibition',
  'GATA1','SPI1','GATA1'
  ),ncol=3)

# Self-loops
motif = t(as.matrix(c('GATA2','activation','GATA2')))
motif = t(as.matrix(c('GATA2','inhibition','GATA2')))

# GATA1-GATA2-SPI1
motif = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/motif_GATA1-SPI1-GATA2.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")
motif = motif[c(-8,-7,-6),] # 92.2 %

# For 1800
motif = rbind(motif,c('GATA2','activation','GATA2'))
motif = motif[c(-8,-7,-6),] # 57 %
motif = motif[c(-7,-6),] # 5 %


# 1.
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/all_scc.Rdata')
# 2.
#load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2activation_degree7_1500/all_scc.Rdata')
# 3. 1600_fixed
load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1600_fixed/all_scc.Rdata')
# 4. 1800_fixed
#load(file='/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1800_fixed/all_scc.Rdata')

scc = lapply(scc,function(x){
  x[x[,2]=='->',2] = 'activation'
  x[x[,2]=='-|',2] = 'inhibition'
  x
})
s.uniq = scc[!duplicated(scc)]


v = lapply(s.uniq,function(x){
  g = unique(c(motif[,1], motif[,3]))
  sub = x[x[,1] %in% g & x[,3] %in% g,]
  a = sprintf("%s__%s__%s",sub[,1],sub[,2],sub[,3])
  m = sprintf("%s__%s__%s",motif[,1],motif[,2],motif[,3])
  all(m %in% a) & all(a %in% m)
})
sum(v==TRUE)/length(v)



######################################## 
## Reduce the profile SCC size by NRD ##
########################################
T = read.csv("/home/satoshi/new/perspective/hematopoietic_data/intersect_RatioDifference_ErythroidAndMyeloid_HSCgse49991.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
net = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500/SCC1_adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv.sif",sep="\t",header=FALSE,stringsAsFactors=FALSE,quote="")

g = rownames(T)[T[,'padj']<=0.05 & abs(T[,'mean.ratio.difference'])>=1 & T[,'padj.1']<=0.05 & abs(T[,'mean.ratio.difference.1'])>=1]
g = toupper(unique(unlist(strsplit(g,"__"))))
N = net[which(net[,1] %in% g & net[,3] %in% g),]
unique(c(net[,1],net[,3]))[which(! unique(c(net[,1],net[,3])) %in% unique(c(N[,1],N[,3])))]

write.table(N, "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_2/SCC1_adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv.sif",sep=" ",row.names=FALSE,quote=FALSE, col.names=FALSE)

########################################### 
## Reduce the profile SCC size by degree ##
###########################################
G = read.csv("/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",header=TRUE,stringsAsFactors=FALSE,quote="")
d = do.call('rbind',lapply(rownames(G),function(x){
  s = sum(G[which(rownames(G) %in% x),]!=0) + sum(G[,which(rownames(G) %in% x)]!=0) 
  if(G[which(rownames(G) %in% x),which(rownames(G) %in% x)] !=0){
    s = s-1
  }
  s
}))
rownames(d) = rownames(G)
write.table(G[which(d>7),which(d>7)], "/home/satoshi/new/perspective/hematopoietic_data/ordered_GATA2unspecified_degree7_1500_2/adjacency_enum_SCC1_metacore_HSC_GSE49991_erythroid_vs_myeloid.csv",sep=" ",row.names=TRUE,quote=FALSE, col.names=TRUE)


###############################
## Extract expression values ##
###############################
## Load the expression
# 1. vsn
expression = read.csv("/home/satoshi/new/perspective/hematopoietic_data/expression_hsc_GSE49991_Erythroid-Myeloid_vsn.csv",sep="\t",header=TRUE,stringsAsFactors=FALSE,quote="")
rownames(expression)[which(rownames(expression) == 'Sfpi1')] = 'Spi1'

genes = c('Gata1','Spi1','Gata2','Cebpa','Fos','Cux1','Irf1')
e = expression[rownames(expression) %in% genes,]
e = cbind(rowMeans(e[,1:3]),rowMeans(e[,4:6]),rowMeans(e[,7:9]))





