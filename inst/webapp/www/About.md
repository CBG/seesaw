## About
---

Identification of cell-fate determinants for directing stem cell differentiation
is one of the challenges in Sytems Biology. Little is known about how cell-fate determinants 
that regulate functionally important subnetworks in large gene-regulatory 
networks (i.e., GRN motifs). Here we provide a generalized 
computational framework for stem cell differentiation that can systematically predict 
cell-fate determinants.

The term 'seesaw' refers to the basic assumption that
that the progenitor cell phenotype is maintained by the dynamic equilibrium of 
the opposing cell-fate determinants, and during binary cell-fate decisions, 
the equilibrium is shifted towards either of the two cell-fate determinants, until finally
the gene expression stabilizes in the corresponding daughter cell type.

This software is made publicly available in the hope that it can guide 
differentiation experiments in stem cell research and regenerative medicine.

### Authors
---

This software was developed in the [Computational Biology Group] (https://wwwfr.uni.lu/lcsb/research/computational_biology) by
- [Andras Hartmann](https://wwwfr.uni.lu/lcsb/people/andras_hartmann)
- [Satoshi Okawa](https://wwwen.uni.lu/lcsb/people/satoshi_okawa)
- [Gaia Zaffaroni](https://wwwen.uni.lu/lcsb/people/gaia_zaffaroni)
- [Antonio del Sol](https://wwwfr.uni.lu/lcsb/people/antonio_del_sol_mesa)

### Citation
---

Within any publication that uses any methods or results derived from or inspired by SeesawPred, please cite:
- <a href="https://www.nature.com/articles/s41598-018-31688-9">Hartmann, A., Okawa, S., Zaffaroni, G., & del Sol, A., 2018. SeesawPred: A Web Application for Predicting Cell-fate Determinants in Cell Differentiation. Scientific Reports 8, 13355</a>
- <a href="https://doi.org/10.1016/j.stemcr.2016.07.014"> Okawa, S., Nicklas, S., Zickenrott, S., Schwamborn, J.C., del Sol, A., 2016. A Generalized Gene-Regulatory Network Model of Stem Cell Differentiation for Predicting Lineage Specifiers. Stem Cell Reports 7, 307–315.</a>
                  
### Examples
---

The examples proviede along with the software are:
-	Mouse Neural Stem Cell (mNSC) differentiation into neurons and astrocytes
- Mouse Hematopoietic Stem Cell (mHSC) differentiation to erythroids myeloids

### Acknowledments
---

 <table style="width:100%">
  <tr>
    <td align="center">
      Icon made by <a title="Freepik" href="http://www.freepik.com">Freepik</a> from <a title="Flaticon" href="http://www.flaticon.com">www.flaticon.com</a>
    </td>
    <td align="center">
      <a href="https://portal.genego.com/" target="_blank">
      Powered by data from Clarivate Analytics
      <br>
      <img src="clarivate.svg" width="128"> </a>
    </td>
  </tr>
</table> 



