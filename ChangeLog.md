# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.5.1] - Current
### Changed
- Delay in loading fixed
- Header image location fixed
- Citation fixed

## [0.5] - 2018-08-01 (Code release)
### Added
- Summary of results (top 10)
- Link to source code

### Changed
- Code cleanup & indentation fix
- License update to AGPL
- Code prepared for sensitivity tests
- bugfix: incorrect results showed in popup textbox

## [0.4.1] 
### Added
- Missing figure in the documentation
- Citation added
- Link to source code

### Changed
- Version of GRNOpt library showed on the page
- Updated manual

## [0.4] - 2018-03-27
### Added
- New examples from seesawData

### Changed
- Fix for datasets without replicates
- Help pages updated
- Application name: SeesawPred

## [0.3.5] official beta
### Added
- New scoring function
- Information page: About

### Changed
- New examples from seesawData

## [0.3.4] - 2018-02-26
### Added
- Error messages in popup window
- Option to choose strategy

### Changed
- New human Prior Knowledge Network
- Output contains upregulated gene
- Expressions displayed in the result
- Data and examples separated
- Examples as select input

## [0.3.3] - 2018-02-06
### Added
- Upload custom Prior Knowledge Network

### Changed
- Updated dataset
- Updated examples
- Ranking based on Min outdegree instead of Mean outdegree

## [0.3.2] - 2018-01-26
### Added
- Mouse TF list from animalDB

### Changed
- Filter SCCs in PKN
- Filter for mouse TFs
- SCC detection can handle unspecified edges
- Updated mouse PKN
- Random sub-sampling from many (10.000) inferred networks

## [0.3.1] - 2018-01-15
### Added
- Support for unspecified edges

### Changed
- Speed-up processing

## [0.3]
### Added
- Shiny web interface
- tag v0.3 - validation

## [0.2]
### Added
- Vignette to reconstruct MSC example

## [0.1]
- Original import
